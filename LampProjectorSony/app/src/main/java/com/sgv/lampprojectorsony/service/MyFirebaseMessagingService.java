package com.sgv.lampprojectorsony.service;



import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sgv.lampprojectorsony.R;
import com.sgv.lampprojectorsony.activity.MainActivity;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMessagingServ";

    @Override
    public void onNewToken(String s) {
        Log.d(TAG, "The token refreshed " + s);
        //sendRegistrationToServer("1", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getFrom());
        String msg = "";
        if (remoteMessage.getData().size() > 0){
            Log.d(TAG, "Message data: " + remoteMessage.getData());
            msg = "WARNING en " + remoteMessage.getData().get("sala") + ". Se alcanzó el siguiente valor: " + remoteMessage.getData().get("lamp");
        }
        if (remoteMessage.getNotification() != null){
            Log.d(TAG, "Message notification body: " + remoteMessage.getNotification().getBody());
            msg = "WARNING en " + remoteMessage.getData().get("sala") + ". Se alcanzó el siguiente valor: " + remoteMessage.getData().get("lamp");
        }
        sendNotification(msg);
    }

    private void sendNotification(String msg) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        Uri soundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "CHANNELLAMP")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("WARNING")
                .setContentText(msg)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);
        NotificationManagerCompat notificationmanager = NotificationManagerCompat.from(this);
        notificationmanager.notify(1, notificationBuilder.build());

    }
}
