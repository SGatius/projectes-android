package com.sgv.lampprojectorsony.model;

public class Lamp {

    private long A1, A2, A3, B1, B2, B3;
    private long auditorium;
    private String documentID;

    public Lamp(long auditorium) {
        this.auditorium = auditorium;
        this.A1 = 0;
        this.A2 = 0;
        this.A3 = 0;
        this.B1 = 0;
        this.B2 = 0;
        this.B3 = 0;
        this.documentID = "";
    }

    public long getAuditorium() {
        return auditorium;
    }

    public long getA1() {
        return A1;
    }

    public void setA1(long a1) {
        A1 = a1;
    }

    public long getA2() {
        return A2;
    }

    public void setA2(long a2) {
        A2 = a2;
    }

    public long getA3() {
        return A3;
    }

    public void setA3(long a3) {
        A3 = a3;
    }

    public long getB1() {
        return B1;
    }

    public void setB1(long b1) {
        B1 = b1;
    }

    public long getB2() {
        return B2;
    }

    public void setB2(long b2) {
        B2 = b2;
    }

    public long getB3() {
        return B3;
    }

    public void setB3(long b3) {
        B3 = b3;
    }

    public String getDocumentID() {
        return documentID;
    }

    public void setDocumentID(String documentID) {
        this.documentID = documentID;
    }
}
