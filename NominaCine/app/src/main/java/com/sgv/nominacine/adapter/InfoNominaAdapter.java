package com.sgv.nominacine.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sgv.nominacine.R;
import com.sgv.nominacine.model.HoursDataWeek;

import java.util.List;

public class InfoNominaAdapter extends ArrayAdapter<HoursDataWeek> {

    private List<HoursDataWeek> data;
    private Context mContext;

    private static class ViewHolder{
        TextView numSetmana, numHores;
    }

    public InfoNominaAdapter(List<HoursDataWeek> data, Context context){
        super(context, R.layout.row_info_nomina, data);
        this.data = data;
        this.mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        HoursDataWeek info = getItem(position);
        ViewHolder viewHolder;
        //final view result;

        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_info_nomina, parent, false);
            viewHolder.numSetmana = (TextView)convertView.findViewById(R.id.text_name_setmana);
            viewHolder.numHores = (TextView)convertView.findViewById(R.id.text_value_hours);
            //result = convertView;
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
            //result = convertView;
        }

        viewHolder.numSetmana.setText(String.valueOf(info.getNumSetmana()));

        viewHolder.numHores.setText(String.format("%.2f",info.getNumHores()) + " hours");
        return convertView;
    }
}
