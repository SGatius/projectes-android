package com.sgv.nominacine.sqlitedb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class TimeWeekContract {

    private TimeWeekContract(){}

    public static class TimeWeek implements BaseColumns {
        public static final String TABLE_NAME = "tabledataweek";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_FECHA = "fecha";
        public static final String COLUMN_NAME_NUMSET = "numSetmana";
        public static final String COLUMN_NAME_NUMHORES = "numHores";
        public static final String COLUMN_NAME_NUMHORESEXTRA = "numHoresExtra";
        public static final String COLUMN_NAME_PLUSSGOLFA = "numPlusGolfa";
        public static final String COLUMN_NAME_COMPLSGOLFA = "numComplGolfa";
        public static final String COLUMN_NAME_HORESFESTIUS = "hFestius";
        public static final String COLUMN_NAME_DIESFESTIUS = "dFestius";
        public static final String COLUMN_NAME_DIESVACANCES = "dVacances";
    }

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + TimeWeek.TABLE_NAME + " (" +
                    TimeWeek.COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    TimeWeek.COLUMN_NAME_FECHA + " TEXT, " +
                    TimeWeek.COLUMN_NAME_NUMSET + " INTEGER, " +
                    TimeWeek.COLUMN_NAME_NUMHORES + " REAL, " +
                    TimeWeek.COLUMN_NAME_NUMHORESEXTRA + " REAL, " +
                    TimeWeek.COLUMN_NAME_PLUSSGOLFA + " INTEGER, " +
                    TimeWeek.COLUMN_NAME_COMPLSGOLFA + " INTEGER, " +
                    TimeWeek.COLUMN_NAME_HORESFESTIUS + " REAL, " +
                    TimeWeek.COLUMN_NAME_DIESFESTIUS + " INTEGER, " +
                    TimeWeek.COLUMN_NAME_DIESVACANCES + " INTEGER)";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + TimeWeek.TABLE_NAME;

    public static class SQLiteDBHelper extends SQLiteOpenHelper {

        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "TimeWeek.db";

        public SQLiteDBHelper(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db){
            db.execSQL(SQL_CREATE_ENTRIES);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
            db.execSQL(SQL_DELETE_ENTRIES);
            onCreate(db);
        }

        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
            onUpgrade(db, oldVersion, newVersion);
        }

    }

}
