package com.sgv.nominacine.activities;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.sgv.nominacine.R;
import com.sgv.nominacine.model.HoursDataWeek;
import com.sgv.nominacine.sqlitedb.TimeWeekContract;

public class AddWeekActivity extends AppCompatActivity {

    private static final String TAG = "Msg de díes: ";
    private static final String msg = "Aquest dia no s'ha fet res";
    private EditText editTextMonth, editTextYear;

    //EditText que contenen les hores de inici i finals de jornada, contant festius, vacances...
    private EditText hinici1m, hinici2m, hfinal1m, hfinal2m,
            hinici1t, hinici2t, hfinal1t, hfinal2t,
            hinici1w, hinici2w, hfinal1w, hfinal2w,
            hinici1th, hinici2th, hfinal1th, hfinal2th,
            hinici1f, hinici2f, hfinal1f, hfinal2f,
            hinici1s, hinici2s, hfinal1s, hfinal2s,
            hinici1sn, hinici2sn, hfinal1sn, hfinal2sn, numFestius, numVacances, hFestius;
    //Switch si s'ha realitzat golfa el dia corresponent o no
    private Switch golfam, golfat, golfaw, golfath, golfaf, golfas, golfasn;

    private int NUMHORESCONTRACTE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_week);

        loadReferencesToWidgets();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (preferences.contains("hoursContract")) {
            NUMHORESCONTRACTE = Integer.parseInt(preferences.getString("hoursContract", ""));
        }else {
            Toast.makeText(this, "If you want use this past, you must put your contract info into tools.", Toast.LENGTH_SHORT).show();
        }

        findViewById(R.id.btn_add_week_db).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveInfoToDB(true);
            }
        });
        findViewById(R.id.fab_add_week_db).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveInfoToDB(false);
            }
        });

    }

    private void saveInfoToDB(Boolean typeBtn) {
        HoursDataWeek data = new HoursDataWeek();
        if (typeBtn){
            if (checkDate()){
                if (saveData()){
                    cleanFields();
                }else {
                    Toast.makeText(this, "Existe algún error en la introducción de horas", Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(this, "El campo fecha está vacio", Toast.LENGTH_SHORT).show();
            }
        }else {
            if (checkDate()){
                if (saveData()){
                    finish();
                }else {
                    Toast.makeText(this, "Existe algún error en la introducción de horas", Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(this, "El campo fecha está vacio", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean saveData(){

        boolean isCorrect = true;
        HoursDataWeek data = new HoursDataWeek();
        String fecha = editTextMonth.getText().toString() + "/" + editTextYear.getText().toString();
        data.setFecha(fecha);

        //Dilluns
        if (!hinici1m.getText().toString().isEmpty()){
            double time = 0;
            if (hfinal1m.getText().toString().isEmpty()){
                return false;
            }else {
                //Controlem si hi han dos horaris o no
                if (hinici2m.getText().toString().isEmpty()){
                    //No es dobla
                    String[] strHourInitM = hinici1m.getText().toString().split(":");
                    String[] strHourFinalM = hfinal1m.getText().toString().split(":");
                    time = (Double.valueOf(strHourFinalM[0]) + Double.valueOf(strHourFinalM[1])/60)
                            -
                            (Double.valueOf(strHourInitM[0]) + Double.valueOf(strHourInitM[1])/60);
                    data.setNumHores(data.getNumHores() + time);

                }else {
                    //Es dobla per la raó que sigui
                    String[] strHourInitM1 = hinici1m.getText().toString().split(":");
                    String[] strHourInitM2 = hinici2m.getText().toString().split(":");
                    String[] strHourFinalM1 = hfinal1m.getText().toString().split(":");
                    String[] strHourFinalM2 = hfinal2m.getText().toString().split(":");
                    time = Double.valueOf(strHourFinalM1[0]) + (Double.valueOf(strHourFinalM1[1])/60);
                    time = time - (Double.valueOf(strHourInitM1[0]) + (Double.valueOf(strHourInitM1[1])/60));
                    time = time + Double.valueOf(strHourFinalM2[0]) + (Double.valueOf(strHourFinalM2[1])/60);
                    time = time - (Double.valueOf(strHourInitM2[0]) + (Double.valueOf(strHourInitM2[1])/60));
                    data.setNumHores(data.getNumHores() + time);
                }
                if (golfam.isChecked()){
                    if (time >= 9){
                        data.setNumPlusSGolfa(data.getNumPlusSGolfa() + 1);
                    }else {
                        data.setNumComplSGolfa(data.getNumComplSGolfa() + 1);
                    }
                }
            }
        }else {
            Log.d(TAG, msg);
        }

        //Dimarts

        if (!hinici1t.getText().toString().isEmpty()){
            double time = 0;
            if (hfinal1t.getText().toString().isEmpty()){
                return false;
            }else {
                //Controlem si hi han dos horaris o no
                if (hinici2t.getText().toString().isEmpty()){
                    //No es dobla
                    String[] strHourInit = hinici1t.getText().toString().split(":");
                    String[] strHourFinal = hfinal1t.getText().toString().split(":");
                    time = (Double.valueOf(strHourFinal[0]) + Double.valueOf(strHourFinal[1])/60)
                            -
                            (Double.valueOf(strHourInit[0]) + Double.valueOf(strHourInit[1])/60);
                }else {
                    //Es dobla per la raó que sigui
                    String[] strHourInit1 = hinici1t.getText().toString().split(":");
                    String[] strHourInit2 = hinici2t.getText().toString().split(":");
                    String[] strHourFinal1 = hfinal1t.getText().toString().split(":");
                    String[] strHourFinal2 = hfinal2t.getText().toString().split(":");
                    time = Double.valueOf(strHourFinal1[0]) + (Double.valueOf(strHourFinal1[1])/60);
                    time = time - (Double.valueOf(strHourInit1[0]) + (Double.valueOf(strHourInit1[1])/60));
                    time = time + Double.valueOf(strHourFinal2[0]) + (Double.valueOf(strHourFinal2[1])/60);
                    time = time - (Double.valueOf(strHourInit2[0]) + (Double.valueOf(strHourInit2[1])/60));
                }
                data.setNumHores(data.getNumHores() + time);

                if (golfat.isChecked()){
                    if (time >= 9){
                        data.setNumPlusSGolfa(data.getNumPlusSGolfa() + 1);
                    }else {
                        data.setNumComplSGolfa(data.getNumComplSGolfa() + 1);
                    }
                }
            }
        }else {
            Log.d(TAG, msg);
        }

        //Dimecres

        if (!hinici1w.getText().toString().isEmpty()){
            double time = 0;
            if (hfinal1w.getText().toString().isEmpty()){
                return false;
            }else {
                //Controlem si hi han dos horaris o no
                if (hinici2w.getText().toString().isEmpty()){
                    //No es dobla
                    String[] strHourInit = hinici1w.getText().toString().split(":");
                    String[] strHourFinal = hfinal1w.getText().toString().split(":");
                    time = (Double.valueOf(strHourFinal[0]) + Double.valueOf(strHourFinal[1])/60)
                            -
                            (Double.valueOf(strHourInit[0]) + Double.valueOf(strHourInit[1])/60);

                }else {
                    //Es dobla per la raó que sigui
                    String[] strHourInit1 = hinici1w.getText().toString().split(":");
                    String[] strHourInit2 = hinici2w.getText().toString().split(":");
                    String[] strHourFinal1 = hfinal1w.getText().toString().split(":");
                    String[] strHourFinal2 = hfinal2w.getText().toString().split(":");
                    time = Double.valueOf(strHourFinal1[0]) + (Double.valueOf(strHourFinal1[1])/60);
                    time = time - (Double.valueOf(strHourInit1[0]) + (Double.valueOf(strHourInit1[1])/60));
                    time = time + Double.valueOf(strHourFinal2[0]) + (Double.valueOf(strHourFinal2[1])/60);
                    time = time - (Double.valueOf(strHourInit2[0]) + (Double.valueOf(strHourInit2[1])/60));

                }
                data.setNumHores(data.getNumHores() + time);
                if (golfaw.isChecked()){
                    if (time >= 9){
                        data.setNumPlusSGolfa(data.getNumPlusSGolfa() + 1);
                    }else {
                        data.setNumComplSGolfa(data.getNumComplSGolfa() + 1);
                    }
                }
            }
        }else {
            Log.d(TAG, msg);
        }

        //Dijous

        if (!hinici1th.getText().toString().isEmpty()){
            double time = 0;
            if (hfinal1th.getText().toString().isEmpty()){
                return false;
            }else {
                //Controlem si hi han dos horaris o no
                if (hinici2th.getText().toString().isEmpty()){
                    //No es dobla
                    String[] strHourInit = hinici1th.getText().toString().split(":");
                    String[] strHourFinal = hfinal1th.getText().toString().split(":");
                    time = (Double.valueOf(strHourFinal[0]) + Double.valueOf(strHourFinal[1])/60)
                            -
                            (Double.valueOf(strHourInit[0]) + Double.valueOf(strHourInit[1])/60);
                }else {
                    //Es dobla per la raó que sigui
                    String[] strHourInit1 = hinici1th.getText().toString().split(":");
                    String[] strHourInit2 = hinici2th.getText().toString().split(":");
                    String[] strHourFinal1 = hfinal1th.getText().toString().split(":");
                    String[] strHourFinal2 = hfinal2th.getText().toString().split(":");
                    time = Double.valueOf(strHourFinal1[0]) + (Double.valueOf(strHourFinal1[1])/60);
                    time = time - (Double.valueOf(strHourInit1[0]) + (Double.valueOf(strHourInit1[1])/60));
                    time = time + Double.valueOf(strHourFinal2[0]) + (Double.valueOf(strHourFinal2[1])/60);
                    time = time - (Double.valueOf(strHourInit2[0]) + (Double.valueOf(strHourInit2[1])/60));
                }
                data.setNumHores(data.getNumHores() + time);
                if (golfath.isChecked()){
                    if (time >= 9){
                        data.setNumPlusSGolfa(data.getNumPlusSGolfa() + 1);
                    }else {
                        data.setNumComplSGolfa(data.getNumComplSGolfa() + 1);
                    }
                }
            }
        }else {
            Log.d(TAG, msg);
        }

        //Divendres

        if (!hinici1f.getText().toString().isEmpty()){
            double time = 0;
            if (hfinal1f.getText().toString().isEmpty()){
                return false;
            }else {
                //Controlem si hi han dos horaris o no
                if (hinici2f.getText().toString().isEmpty()){
                    //No es dobla
                    String[] strHourInit = hinici1f.getText().toString().split(":");
                    String[] strHourFinal = hfinal1f.getText().toString().split(":");
                    time = (Double.valueOf(strHourFinal[0]) + Double.valueOf(strHourFinal[1])/60)
                            -
                            (Double.valueOf(strHourInit[0]) + Double.valueOf(strHourInit[1])/60);
                }else {
                    //Es dobla per la raó que sigui
                    String[] strHourInit1 = hinici1f.getText().toString().split(":");
                    String[] strHourInit2 = hinici2f.getText().toString().split(":");
                    String[] strHourFinal1 = hfinal1f.getText().toString().split(":");
                    String[] strHourFinal2 = hfinal2f.getText().toString().split(":");
                    time = Double.valueOf(strHourFinal1[0]) + (Double.valueOf(strHourFinal1[1])/60);
                    time = time - (Double.valueOf(strHourInit1[0]) + (Double.valueOf(strHourInit1[1])/60));
                    time = time + Double.valueOf(strHourFinal2[0]) + (Double.valueOf(strHourFinal2[1])/60);
                    time = time - (Double.valueOf(strHourInit2[0]) + (Double.valueOf(strHourInit2[1])/60));
                }
                data.setNumHores(data.getNumHores() + time);
                if (golfaf.isChecked()){
                    if (time >= 9){
                        data.setNumPlusSGolfa(data.getNumPlusSGolfa() + 1);
                    }else {
                        data.setNumComplSGolfa(data.getNumComplSGolfa() + 1);
                    }
                }
            }
        }else {
            Log.d(TAG, msg);
        }

        //Dissabte

        if (!hinici1s.getText().toString().isEmpty()){
            double time = 0;
            if (hfinal1s.getText().toString().isEmpty()){
                return false;
            }else {
                //Controlem si hi han dos horaris o no
                if (hinici2s.getText().toString().isEmpty()){
                    //No es dobla
                    String[] strHourInit = hinici1s.getText().toString().split(":");
                    String[] strHourFinal = hfinal1s.getText().toString().split(":");
                    time = (Double.valueOf(strHourFinal[0]) + Double.valueOf(strHourFinal[1])/60)
                            -
                            (Double.valueOf(strHourInit[0]) + Double.valueOf(strHourInit[1])/60);
                }else {
                    //Es dobla per la raó que sigui
                    String[] strHourInit1 = hinici1s.getText().toString().split(":");
                    String[] strHourInit2 = hinici2s.getText().toString().split(":");
                    String[] strHourFinal1 = hfinal1s.getText().toString().split(":");
                    String[] strHourFinal2 = hfinal2s.getText().toString().split(":");
                    time = Double.valueOf(strHourFinal1[0]) + (Double.valueOf(strHourFinal1[1])/60);
                    time = time - (Double.valueOf(strHourInit1[0]) + (Double.valueOf(strHourInit1[1])/60));
                    time = time + Double.valueOf(strHourFinal2[0]) + (Double.valueOf(strHourFinal2[1])/60);
                    time = time - (Double.valueOf(strHourInit2[0]) + (Double.valueOf(strHourInit2[1])/60));

                }
                data.setNumHores(data.getNumHores() + time);
                if (golfas.isChecked()){
                    if (time >= 9){
                        data.setNumPlusSGolfa(data.getNumPlusSGolfa() + 1);
                    }else {
                        data.setNumComplSGolfa(data.getNumComplSGolfa() + 1);
                    }
                }
            }
        }else {
            Log.d(TAG, msg);
        }

        //Diumenge

        if (!hinici1sn.getText().toString().isEmpty()){
            double time = 0;
            if (hfinal1sn.getText().toString().isEmpty()){
                return false;
            }else {
                //Controlem si hi han dos horaris o no
                if (hinici2sn.getText().toString().isEmpty()){
                    //No es dobla
                    String[] strHourInit = hinici1sn.getText().toString().split(":");
                    String[] strHourFinal = hfinal1sn.getText().toString().split(":");
                    time = (Double.valueOf(strHourFinal[0]) + Double.valueOf(strHourFinal[1])/60)
                            -
                            (Double.valueOf(strHourInit[0]) + Double.valueOf(strHourInit[1])/60);

                }else {
                    //Es dobla per la raó que sigui
                    String[] strHourInit1 = hinici1sn.getText().toString().split(":");
                    String[] strHourInit2 = hinici2sn.getText().toString().split(":");
                    String[] strHourFinal1 = hfinal1sn.getText().toString().split(":");
                    String[] strHourFinal2 = hfinal2sn.getText().toString().split(":");
                    time = Double.valueOf(strHourFinal1[0]) + (Double.valueOf(strHourFinal1[1])/60);
                    time = time - (Double.valueOf(strHourInit1[0]) + (Double.valueOf(strHourInit1[1])/60));
                    time = time + Double.valueOf(strHourFinal2[0]) + (Double.valueOf(strHourFinal2[1])/60);
                    time = time - (Double.valueOf(strHourInit2[0]) + (Double.valueOf(strHourInit2[1])/60));
                }
                data.setNumHores(data.getNumHores() + time);
                if (golfasn.isChecked()){
                    if (time >= 9){
                        data.setNumPlusSGolfa(data.getNumPlusSGolfa() + 1);
                    }else {
                        data.setNumComplSGolfa(data.getNumComplSGolfa() + 1);
                    }
                }
            }
        }else {
            Log.d(TAG, msg);
        }
        if (!numFestius.getText().toString().isEmpty()){
            data.setdFestius(Integer.parseInt(numFestius.getText().toString()));
        }
        if (!numVacances.getText().toString().isEmpty()){
            data.setdVacances(Integer.parseInt(numVacances.getText().toString()));
        }
        if (!hFestius.getText().toString().isEmpty()){
            data.sethFestius(Double.parseDouble(hFestius.getText().toString()));
        }

        double baseXSetmana;
        if (data.getdVacances() == 7){
            baseXSetmana = 0;
        }else {
            baseXSetmana = NUMHORESCONTRACTE -
                    (NUMHORESCONTRACTE * data.getdFestius() / 6) -
                    (NUMHORESCONTRACTE * data.getdVacances() / 6);
        }
        data.setNumHoresExtra(data.getNumHores() - baseXSetmana - 2 * (data.getNumPlusSGolfa() + data.getNumComplSGolfa()));

        Toast.makeText(this, "HOLA", Toast.LENGTH_SHORT).show();
        if (isCorrect){
            TimeWeekContract.SQLiteDBHelper dbHelper = new TimeWeekContract.SQLiteDBHelper(getApplicationContext());
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            String[] projection = {
                    TimeWeekContract.TimeWeek.COLUMN_NAME_NUMSET
            };
            String selection = TimeWeekContract.TimeWeek.COLUMN_NAME_FECHA + " = ?";
            String[] selectionArgs = {fecha};
            String sortOrder = TimeWeekContract.TimeWeek.COLUMN_NAME_NUMSET + " ASC";

            Cursor cursor = db.query(TimeWeekContract.TimeWeek.TABLE_NAME,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    sortOrder);

            if (cursor.getCount() > 0){
                cursor.moveToNext();
                data.setNumSetmana(cursor.getInt(cursor.getColumnIndexOrThrow(TimeWeekContract.TimeWeek.COLUMN_NAME_NUMSET)) + 1);
            }else {
                data.setNumSetmana(1);
            }

            ContentValues values = new ContentValues();
            values.put(TimeWeekContract.TimeWeek.COLUMN_NAME_FECHA, data.getFecha());
            values.put(TimeWeekContract.TimeWeek.COLUMN_NAME_NUMSET, data.getNumSetmana());
            values.put(TimeWeekContract.TimeWeek.COLUMN_NAME_NUMHORES, data.getNumHores());
            values.put(TimeWeekContract.TimeWeek.COLUMN_NAME_NUMHORESEXTRA, data.getNumHoresExtra());
            values.put(TimeWeekContract.TimeWeek.COLUMN_NAME_PLUSSGOLFA, data.getNumPlusSGolfa());
            values.put(TimeWeekContract.TimeWeek.COLUMN_NAME_COMPLSGOLFA, data.getNumComplSGolfa());
            values.put(TimeWeekContract.TimeWeek.COLUMN_NAME_HORESFESTIUS, data.gethFestius());
            values.put(TimeWeekContract.TimeWeek.COLUMN_NAME_DIESFESTIUS, data.getdFestius());
            values.put(TimeWeekContract.TimeWeek.COLUMN_NAME_DIESVACANCES, data.getdVacances());
            db.insert(TimeWeekContract.TimeWeek.TABLE_NAME, null, values);
        }
        return isCorrect;

    }

    private boolean checkDate(){
        if (editTextMonth.getText().toString().isEmpty() || editTextYear.getText().toString().isEmpty()){
            return false;
        }else {
            return true;
        }
    }

    private void cleanFields(){
        hinici1m.setText("");
        hinici2m.setText("");
        hfinal1m.setText("");
        hfinal2m.setText("");
        golfam.setChecked(false);

        hinici1t.setText("");
        hinici2t.setText("");
        hfinal1t.setText("");
        hfinal2t.setText("");
        golfat.setChecked(false);

        hinici1w.setText("");
        hinici2w.setText("");
        hfinal1w.setText("");
        hfinal2w.setText("");
        golfaw.setChecked(false);

        hinici1th.setText("");
        hinici2th.setText("");
        hfinal1th.setText("");
        hfinal2th.setText("");
        golfath.setChecked(false);

        hinici1f.setText("");
        hinici2f.setText("");
        hfinal1f.setText("");
        hfinal2f.setText("");
        golfaf.setChecked(false);

        hinici1s.setText("");
        hinici2s.setText("");
        hfinal1s.setText("");
        hfinal2s.setText("");
        golfas.setChecked(false);

        hinici1sn.setText("");
        hinici2sn.setText("");
        hfinal1sn.setText("");
        hfinal2sn.setText("");
        golfasn.setChecked(false);
    }

    private void loadReferencesToWidgets() {

        editTextMonth = findViewById(R.id.edit_text_month);
        editTextYear = findViewById(R.id.edit_text_year);

        hinici1m = findViewById(R.id.hinicim1);
        hinici2m = findViewById(R.id.hinicim2);
        hfinal1m = findViewById(R.id.hfinalm1);
        hfinal2m = findViewById(R.id.hfinalm2);
        golfam = findViewById(R.id.ngolfam);

        hinici1t = findViewById(R.id.hinicit1);
        hinici2t = findViewById(R.id.hinicit2);
        hfinal1t = findViewById(R.id.hfinalt1);
        hfinal2t = findViewById(R.id.hfinalt2);
        golfat = findViewById(R.id.ngolfat);

        hinici1w = findViewById(R.id.hiniciw1);
        hinici2w = findViewById(R.id.hiniciw2);
        hfinal1w = findViewById(R.id.hfinalw1);
        hfinal2w = findViewById(R.id.hfinalw2);
        golfaw = findViewById(R.id.ngolfaw);

        hinici1th = findViewById(R.id.hinicith1);
        hinici2th = findViewById(R.id.hinicith2);
        hfinal1th = findViewById(R.id.hfinalth1);
        hfinal2th = findViewById(R.id.hfinalth2);
        golfath = findViewById(R.id.ngolfath);

        hinici1f = findViewById(R.id.hinicif1);
        hinici2f = findViewById(R.id.hinicif2);
        hfinal1f = findViewById(R.id.hfinalf1);
        hfinal2f = findViewById(R.id.hfinalf2);
        golfaf = findViewById(R.id.ngolfaf);

        hinici1s = findViewById(R.id.hinicis1);
        hinici2s = findViewById(R.id.hinicis2);
        hfinal1s = findViewById(R.id.hfinals1);
        hfinal2s = findViewById(R.id.hfinals2);
        golfas = findViewById(R.id.ngolfas);

        hinici1sn = findViewById(R.id.hinicisn1);
        hinici2sn = findViewById(R.id.hinicisn2);
        hfinal1sn = findViewById(R.id.hfinalsn1);
        hfinal2sn = findViewById(R.id.hfinalsn2);
        golfasn = findViewById(R.id.ngolfasn);

        numFestius = findViewById(R.id.num_festius);
        hFestius = findViewById(R.id.num_h_festius);
        numVacances = findViewById(R.id.num_vacances);
    }
}
