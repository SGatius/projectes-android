package com.sgv.nominacine.activities;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.sgv.nominacine.R;
import com.sgv.nominacine.adapter.InfoNominaAdapter;
import com.sgv.nominacine.model.HoursDataWeek;
import com.sgv.nominacine.sqlitedb.TimeWeekContract;

import java.util.ArrayList;
import java.util.List;

public class InfoNomina extends AppCompatActivity {

    private List<HoursDataWeek> hoursDataWeekList;
    private SQLiteDatabase db;
    private InfoNominaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_nomina);
        ListView listView = findViewById(R.id.list_setmanes);
        hoursDataWeekList = new ArrayList<>();
        String fecha = getIntent().getStringExtra("FECHA");

        TimeWeekContract.SQLiteDBHelper dbHelper = new TimeWeekContract.SQLiteDBHelper(getApplicationContext());
        db = dbHelper.getWritableDatabase();

        getWeekByData(fecha);

        adapter = new InfoNominaAdapter(hoursDataWeekList, this);
        listView.setAdapter(adapter);

        FloatingActionButton fab = findViewById(R.id.fab_generate_nomina);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateNominaToDB();
            }
        });
    }

    private void generateNominaToDB() {

    }

    private void getWeekByData(String fecha) {
        String[] projection = {
                TimeWeekContract.TimeWeek.COLUMN_NAME_ID,
                TimeWeekContract.TimeWeek.COLUMN_NAME_FECHA,
                TimeWeekContract.TimeWeek.COLUMN_NAME_NUMSET,
                TimeWeekContract.TimeWeek.COLUMN_NAME_NUMHORES,
                TimeWeekContract.TimeWeek.COLUMN_NAME_NUMHORESEXTRA,
                TimeWeekContract.TimeWeek.COLUMN_NAME_PLUSSGOLFA,
                TimeWeekContract.TimeWeek.COLUMN_NAME_COMPLSGOLFA,
                TimeWeekContract.TimeWeek.COLUMN_NAME_HORESFESTIUS,
                TimeWeekContract.TimeWeek.COLUMN_NAME_DIESFESTIUS,
                TimeWeekContract.TimeWeek.COLUMN_NAME_DIESVACANCES
        };
        String selection = TimeWeekContract.TimeWeek.COLUMN_NAME_FECHA + " = ?";
        String[] selectionArgs = {fecha};
        String sortOrder = TimeWeekContract.TimeWeek.COLUMN_NAME_NUMSET + " DESC";

        Cursor cursor = db.query(TimeWeekContract.TimeWeek.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);

        while (cursor.moveToNext()){
            HoursDataWeek data = new HoursDataWeek(
                    cursor.getString(cursor.getColumnIndexOrThrow(TimeWeekContract.TimeWeek.COLUMN_NAME_FECHA)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(TimeWeekContract.TimeWeek.COLUMN_NAME_NUMSET)),
                    cursor.getDouble(cursor.getColumnIndexOrThrow(TimeWeekContract.TimeWeek.COLUMN_NAME_NUMHORES)),
                    cursor.getDouble(cursor.getColumnIndexOrThrow(TimeWeekContract.TimeWeek.COLUMN_NAME_NUMHORESEXTRA)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(TimeWeekContract.TimeWeek.COLUMN_NAME_PLUSSGOLFA)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(TimeWeekContract.TimeWeek.COLUMN_NAME_COMPLSGOLFA)),
                    cursor.getDouble(cursor.getColumnIndexOrThrow(TimeWeekContract.TimeWeek.COLUMN_NAME_HORESFESTIUS)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(TimeWeekContract.TimeWeek.COLUMN_NAME_DIESFESTIUS)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(TimeWeekContract.TimeWeek.COLUMN_NAME_DIESVACANCES))
            );
            data.setId(cursor.getInt(cursor.getColumnIndexOrThrow(TimeWeekContract.TimeWeek.COLUMN_NAME_ID)));
            hoursDataWeekList.add(data);
        }
    }

}
