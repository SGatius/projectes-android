package com.sgv.nominacine.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.sgv.nominacine.R;
import com.sgv.nominacine.sqlitedb.TimeWeekContract;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;
    private ListView listNomines;
    private List<String> listDates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        listNomines = findViewById(R.id.list_nomines);
        listDates = new ArrayList<>();
        TimeWeekContract.SQLiteDBHelper dbHelper = new TimeWeekContract.SQLiteDBHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String[] projection = {
                TimeWeekContract.TimeWeek.COLUMN_NAME_FECHA
        };
        String selection = TimeWeekContract.TimeWeek.COLUMN_NAME_FECHA + " = ?";
        String[] selectionArgs = {};
        String sortOrder = TimeWeekContract.TimeWeek.COLUMN_NAME_NUMSET + " ASC";

        Cursor cursor = db.query(TimeWeekContract.TimeWeek.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                sortOrder);

        while (cursor.moveToNext()){
            listDates.add(cursor.getString(cursor.getColumnIndexOrThrow(TimeWeekContract.TimeWeek.COLUMN_NAME_FECHA)));
        }
        listDates.add("");
        HashSet<String> hashSet = new HashSet<>();
        hashSet.addAll(listDates);
        listDates.clear();
        listDates.addAll(hashSet);
        ArrayAdapter adapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, listDates);
        listNomines.setAdapter(adapter);

        listNomines.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, InfoNomina.class);
                intent.putExtra("FECHA", listDates.get(position));
                startActivity(intent);
            }
        });

        findViewById(R.id.fab_add_week).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AddWeekActivity.class));
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        );
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.nav_info:
                startActivity(new Intent(this, InfoSalaryActivity.class));
                break;
            case R.id.nav_manage:
                startActivity(new Intent(this, ConfigurationUser.class));
                break;
            case R.id.nav_share:

                break;
            case R.id.nav_send:

                break;
            default:
                throw new IllegalArgumentException("menu option not implemented!!");
        }

        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }
}
