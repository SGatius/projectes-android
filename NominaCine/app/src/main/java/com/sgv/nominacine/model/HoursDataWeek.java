package com.sgv.nominacine.model;

public class HoursDataWeek {
    private int id;
    private String fecha;
    private int numSetmana;
    private double numHores;
    private double numHoresExtra;
    private int numPlusSGolfa;
    private int numComplSGolfa;
    private double hFestius;
    private int dFestius;
    private int dVacances;

    public HoursDataWeek(String fecha, int numSetmana, double numHores, double numHoresExtra, int numPlusSGolfa, int numComplSGolfa, double hFestius, int dFestius, int dVacances) {
        this.fecha = fecha;
        this.numSetmana = numSetmana;
        this.numHores = numHores;
        this.numHoresExtra = numHoresExtra;
        this.numPlusSGolfa = numPlusSGolfa;
        this.numComplSGolfa = numComplSGolfa;
        this.hFestius = hFestius;
        this.dFestius = dFestius;
        this.dVacances = dVacances;
    }

    public HoursDataWeek(){
        this.fecha = "";
        this.numHores = 0;
        this.numHoresExtra = 0;
        this.numPlusSGolfa = 0;
        this.numComplSGolfa = 0;
        this.hFestius = 0;
        this.dFestius = 0;
        this.dVacances = 0;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getNumSetmana() {
        return numSetmana;
    }

    public void setNumSetmana(int numSetmana) {
        this.numSetmana = numSetmana;
    }

    public double getNumHores() {
        return numHores;
    }

    public void setNumHores(double numHores) {
        this.numHores = numHores;
    }

    public double getNumHoresExtra() {
        return numHoresExtra;
    }

    public void setNumHoresExtra(double numHoresExtra) {
        this.numHoresExtra = numHoresExtra;
    }

    public int getNumPlusSGolfa() {
        return numPlusSGolfa;
    }

    public void setNumPlusSGolfa(int numPlusSGolfa) {
        this.numPlusSGolfa = numPlusSGolfa;
    }

    public int getNumComplSGolfa() {
        return numComplSGolfa;
    }

    public void setNumComplSGolfa(int numComplSGolfa) {
        this.numComplSGolfa = numComplSGolfa;
    }

    public double gethFestius() {
        return hFestius;
    }

    public void sethFestius(double hFestius) {
        this.hFestius = hFestius;
    }

    public int getdFestius() {
        return dFestius;
    }

    public void setdFestius(int dFestius) {
        this.dFestius = dFestius;
    }

    public int getdVacances() {
        return dVacances;
    }

    public void setdVacances(int dVacances) {
        this.dVacances = dVacances;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
