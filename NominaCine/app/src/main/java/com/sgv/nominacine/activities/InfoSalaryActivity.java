package com.sgv.nominacine.activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.sgv.nominacine.R;
import com.sgv.nominacine.adapter.InfoSalaryAdapter;
import com.sgv.nominacine.model.InfoSalary;

import java.util.ArrayList;
import java.util.List;

public class InfoSalaryActivity extends AppCompatActivity {

    private List<InfoSalary> data;
    private InfoSalaryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_salary);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Info salary");
        setSupportActionBar(toolbar);

        ListView listView = findViewById(R.id.list_salary_info);
        data = new ArrayList<>();
        adapter = new InfoSalaryAdapter(data, getApplicationContext());
        listView.setAdapter(adapter);

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("infosalary")
                .document("salary")
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()){
                            data.add(new InfoSalary("Salari anual brut (1,2)",
                                    task.getResult().getDouble("salarianualbrut12")));
                            data.add(new InfoSalary("Preu/Hora(1,2)",
                                    task.getResult().getDouble("preuhora12")));
                            data.add(new InfoSalary("Salari anual brut (3)",
                                    task.getResult().getDouble("salarianualbrut3")));
                            data.add(new InfoSalary("Preu/Hora(3)",
                                    task.getResult().getDouble("preuhora3")));
                            data.add(new InfoSalary("Trienni",
                                    task.getResult().getDouble("trienni")));
                            data.add(new InfoSalary("Salari base mensual(1,2)",
                                    task.getResult().getDouble("salaribasem12")));
                            data.add(new InfoSalary("Salari base mensual(3)",
                                    task.getResult().getDouble("salaribasem3")));
                            data.add(new InfoSalary("Paga extra(1,2)",
                                    task.getResult().getDouble("pagaextra12")));
                            data.add(new InfoSalary("Paga extra(3)",
                                    task.getResult().getDouble("pagaextra3")));
                            data.add(new InfoSalary("Plus sessió golfa(1,2)",
                                    task.getResult().getDouble("plusgolfa12")));
                            data.add(new InfoSalary("Plus sessió golfa(3)",
                                    task.getResult().getDouble("plusgolfa3")));
                            data.add(new InfoSalary("Recàrrec golfa(1,2)",
                                    task.getResult().getDouble("recarrecgolfa12")));
                            data.add(new InfoSalary("Recarrec golfa(3)",
                                    task.getResult().getDouble("recarrecgolfa3")));

                            adapter.notifyDataSetChanged();

                        }
                    }
                });

    }
}
