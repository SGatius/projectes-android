package com.sgv.nominacine.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sgv.nominacine.R;
import com.sgv.nominacine.model.InfoSalary;

import java.util.List;

public class InfoSalaryAdapter extends ArrayAdapter<InfoSalary>{

    private List<InfoSalary> dataSet;
    Context mContext;

    private static class ViewHolder{
        TextView name, value;
    }

    public InfoSalaryAdapter(List<InfoSalary> data, Context context){
        super(context, R.layout.row_info_salary, data);
        this.dataSet = data;
        this.mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        InfoSalary infoSalary = getItem(position);
        ViewHolder viewHolder;
        //final view result;

        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_info_salary, parent, false);
            viewHolder.name = (TextView)convertView.findViewById(R.id.text_name_salary);
            viewHolder.value = (TextView)convertView.findViewById(R.id.text_value_salary);
            //result = convertView;
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
            //result = convertView;
        }

        viewHolder.name.setText(infoSalary.getName());
        viewHolder.value.setText(String.valueOf(infoSalary.getValue()));
        return convertView;
    }
}
