package com.sgv.discinegenerator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomAdapter extends BaseAdapter {

    static class ViewHolder{
        ImageView imageView;
        TextView textView;
    }

    private List listNameFiles;
    private LayoutInflater inflater = null;

    public CustomAdapter(Context context, List data){
        this.listNameFiles = data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getCount() {
        return listNameFiles.size();
    }

    @Override
    public Object getItem(int position) {
        return listNameFiles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            convertView = inflater.inflate(R.layout.row_iteml, null);
            holder = new ViewHolder();
            holder.imageView = (ImageView)convertView.findViewById(R.id.type_file);
            holder.textView = (TextView)convertView.findViewById(R.id.name_file);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        String name = getItem(position).toString();
        if (name.endsWith("csv")){
            holder.imageView.setImageResource(R.drawable.ic_excel);
        }else{
            holder.imageView.setImageResource(R.drawable.ic_pdf);
        }
        holder.textView.setText(listNameFiles.get(position).toString());

        return convertView;
    }
}
