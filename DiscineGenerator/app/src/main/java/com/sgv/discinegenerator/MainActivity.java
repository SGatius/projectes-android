package com.sgv.discinegenerator;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tom_roush.pdfbox.pdmodel.PDDocument;
import com.tom_roush.pdfbox.text.PDFTextStripper;
import com.tom_roush.pdfbox.text.PDFTextStripperByArea;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_FILES = 7193;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_FILES = 21367;
    private List listNameFiles, listDirectoryFiles;
    private String rootDirectory;
    private ListView listView;
    private CustomAdapter customAdapter;
    private HashMap<List<Integer>, List<String>> mapDiscines;
    private List<Integer> listAuditorium;
    private List<String> listDiscines;
    private List<String> listDiscinesToPush;
    private Handler mHandler;
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mHandler = new Handler();
        mapDiscines = new HashMap<>();
        rootDirectory = Environment.getExternalStorageDirectory().getPath() + "/" + Environment.DIRECTORY_DOWNLOADS;

        listView = findViewById(R.id.list_directory_files);
        progressBar = findViewById(R.id.progressBar);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_FILES);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else{
            showFilesDirectory(rootDirectory);
        }

        listDiscinesToPush = new ArrayList<>();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                File file = new File(listDirectoryFiles.get(position).toString());
                String directory = file.getParent();
                if (file.isFile()) {
                    if (file.getName().contains(".pdf")) {
                        if (checkWritePermissionItsOk()) {
                            startProcess(file, directory);
                        }else{
                            askPermissionToWrite();
                        }
                    } else if(file.getName().contains(".csv")){
                        Uri uri = FileProvider.getUriForFile(MainActivity.this, "com.sgv.discinegenerator.FileProvider", file);
                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.setType("text/plain");
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "DISCINE GENERATOR");
                        emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
                        emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        try {
                            startActivity(Intent.createChooser(emailIntent, "Enviar email..."));
                            finish();
                        } catch (android.content.ActivityNotFoundException ex) {
                            Toast.makeText(MainActivity.this,
                                    "No tienes clientes de email instalados.", Toast.LENGTH_SHORT).show();
                            Log.e("SEND MAIL", "L'usuari no té client de mail");
                        }
                    }else {
                        Toast.makeText(MainActivity.this, "El document que sel·leccioni ha de ser un document pdf per ser processat o bé un csv per envia'l per correu", Toast.LENGTH_LONG).show();
                    }
                } else {
                    showFilesDirectory(listDirectoryFiles.get(position).toString());
                }
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final File file = new File(listDirectoryFiles.get(position).toString());
                listDirectoryFiles.remove(position);
                listNameFiles.remove(file.getName());
                customAdapter.notifyDataSetChanged();
                Snackbar snackbar = Snackbar.make(view, file.getName(), Snackbar.LENGTH_LONG)
                        .setAction("Undo", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                listNameFiles.add(file.getName());
                                listDirectoryFiles.add(file.getPath());
                                customAdapter.notifyDataSetChanged();
                            }
                        });
                snackbar.show();
                snackbar.addCallback(new Snackbar.Callback(){
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        if (!listNameFiles.contains(file.getName())){
                            file.delete();
                        }
                    }
                });
                return true;
            }
        });

    }

    private void startProcess(final File file, final String directory){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mHandler.post(new Runnable() {
                    @Override
                        public void run() {

                            //Aqui es mostraria el progress dialog
                            progressBar.setVisibility(View.VISIBLE);
                            Log.d("Check", "Es comprova que el fitxer no estigui encriptat");
                        }
                    });
                    PDDocument document = PDDocument.load(file);
                    document.getClass();

                    if (!document.isEncrypted()) {
                        PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                        stripper.setSortByPosition(true);
                        PDFTextStripper tStripper = new PDFTextStripper();
                        readAndTreatToDiscine(tStripper.getText(document));
                    } else {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(MainActivity.this, "El document està encriptat, no el podem llegir. L'has de desencriptar", Toast.LENGTH_SHORT).show();
                                Log.e("Check", "L'usuari ha intentat processar un fitxer encriptat");
                            }
                        });
                    }
                    document.close();

                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if(file.delete()) {
                                    listNameFiles.remove(file.getName());
                                    listDirectoryFiles.remove(file.getAbsolutePath());
                                    customAdapter.notifyDataSetChanged();
                                    Log.d("PROCESS", "S'ha eliminat el document després de ser processat");
                                }else {
                                    Log.e("PROCESS", "No s'ha pogut eliminar el document");
                                }
                                Thread.sleep(1500);
                            }catch (InterruptedException e){
                                Log.e("INTERRUPTED EXCEPTION", "Excepció al dormir el fil en un temps de 1.5 segons");
                            }
                            Log.d("Process", "S'ha finalitzat el processament del fitxer, es procedeix a l'escriptura dels resultats");
                        }
                    });
                    writeToCSV(directory);

                    mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, "File processed", Toast.LENGTH_LONG).show(); }
                        });
                } catch (IOException e) {
                    Log.e("Error lectura", e.getMessage());
                }
            }
        }).start();
    }

    private void showFilesDirectory(String directory) {

        listNameFiles = new ArrayList();
        listDirectoryFiles = new ArrayList();
        File currentDirectory = new File(directory);
        File[] listFile = currentDirectory.listFiles();

        for (File file : listFile){
            if (file.isFile()){
                if (file.getName().endsWith("csv") || file.getName().endsWith("pdf")) {
                    listDirectoryFiles.add(file.getPath());
                }
            }
        }
        Collections.sort(listDirectoryFiles, String.CASE_INSENSITIVE_ORDER);
        for (int i = 0; i < listDirectoryFiles.size(); i++){
            File file = new File(listDirectoryFiles.get(i).toString());
            if (file.isFile()){
                listNameFiles.add(file.getName());
            }
        }
        if (listFile.length < 1){
            listNameFiles.add("No hi ha ningún arxiu");
            listDirectoryFiles.add(directory);
        }
        customAdapter = new CustomAdapter(this, listNameFiles);
        listView.setAdapter(customAdapter);
    }

    private void writeToCSV(String directoryToWrite){
        DateTime dateTime = DateTime.now();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd-MM-yyyy");
        final String nameFile = dateTime.toString(fmt) + ".csv";
        directoryToWrite = directoryToWrite + "/" + nameFile;
        final String newDirectory = directoryToWrite;
        Collections.sort(listDiscinesToPush);

        try {
            PrintWriter csvFile = new PrintWriter(directoryToWrite);

            Set<List<Integer>> set = mapDiscines.keySet();

            Iterator<List<Integer>> iterator = set.iterator();
            while(iterator.hasNext()){
                List<Integer> temp = iterator.next();
                String aText = "";
                for(int i = 0; i < temp.size(); i++){
                    if(i == 0){
                        aText = temp.get(i).toString();
                    }else{
                        aText = aText + " | " + temp.get(i).toString();
                    }
                }
                csvFile.println(aText);
                String textDiscines = "";
                for (int i = 0; i < mapDiscines.get(temp).size(); i++){
                    textDiscines = textDiscines + mapDiscines.get(temp).get(i) + ",";
                    if (i % 5 == 0 && i != 0){
                        csvFile.println(textDiscines);
                        textDiscines = "";
                    }else if (i == mapDiscines.get(temp).size() - 1){
                        csvFile.println(textDiscines);
                    }
                }
            }
            csvFile.println();
            csvFile.println("LLISTA DE DISCINES");
            String listTextDiscines = "";
            for (int i = 0; i < listDiscinesToPush.size(); i++){
                listTextDiscines = listTextDiscines + listDiscinesToPush.get(i) + ",";
                if (i % 5 == 0 && i != 0){
                    csvFile.println(listTextDiscines);
                    listTextDiscines = "";
                }else if (i == listDiscinesToPush.size() - 1){
                    csvFile.println(listTextDiscines);
                }
            }
            csvFile.close();

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    listNameFiles.add(nameFile);
                    listDirectoryFiles.add(newDirectory);
                    customAdapter.notifyDataSetChanged();
                    progressBar.setVisibility(View.GONE);
                }
            });

        }catch (FileNotFoundException e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private void readAndTreatToDiscine(String str){

        Scanner scnLine = new Scanner(str);
        String line = "";
        int currentAuditorium = 0;

        while (scnLine.hasNextLine()){
            line = scnLine.nextLine();
            if (line.contains("Pantalla:")){
                listAuditorium = new ArrayList<>();
                listDiscines = new ArrayList<>();
                Scanner scnWord = new Scanner(line);
                scnWord.next();
                scnWord.next();
                scnWord.next();
                scnWord.next();
                currentAuditorium = scnWord.nextInt();
                listAuditorium.add(currentAuditorium);
            }else if (line.contains("DN2") || line.contains("DL2")){
                Scanner scnWord = new Scanner(line);
                String discine = scnWord.next();

                listDiscines.add(discine);
                if (!listDiscinesToPush.contains(discine)){
                    listDiscinesToPush.add(discine);
                }
            }else if (line.contains("En caso de que exista")){
                if(mapDiscines.containsValue(listDiscines)){
                    List<List<Integer>> aux = new ArrayList<>();
                    aux.add(listAuditorium);
                    for(Map.Entry<List<Integer> , List<String>> tmp : mapDiscines.entrySet()){
                        if(tmp.getValue().equals(listDiscines)){
                            aux.add(tmp.getKey());
                            listAuditorium.addAll(tmp.getKey());
                        }
                    }
                    for(int i = 1; i < aux.size(); i++){
                        mapDiscines.remove(aux.get(i));
                    }
                    Collections.sort(listAuditorium);
                    mapDiscines.put(listAuditorium, listDiscines);
                }else{
                    mapDiscines.put(listAuditorium, listDiscines);
                }
            }

        }
    }

    private boolean checkWritePermissionItsOk(){
        boolean permission = false;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            permission = true;
        }
        return permission;
    }
    
    private void askPermissionToWrite(){
        Toast.makeText(this, "You must accept the permission to write the file into your memory", Toast.LENGTH_LONG).show();
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            // Show an expanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.

        } else {

            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_FILES);
            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
    }
    
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_FILES: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showFilesDirectory(rootDirectory);
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(this, "You must accept the request permission for use this app.", Toast.LENGTH_SHORT).show();
                    finishAffinity();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_WRITE_FILES: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Now, you can select the file to process.", Toast.LENGTH_SHORT).show();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(this, "You must accept the request permission to put the file to process.", Toast.LENGTH_SHORT).show();
                    finishAffinity();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.upload_data:
                uploadData();
                return true;
            case R.id.show_current_discines:
                showCurrentDiscines();
                return true;
            case R.id.show_discines_to_delete:
                showDiscinesToDelete();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showDiscinesToDelete(){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("DISCINES");
        Query getDiscines = myRef.orderByKey().limitToLast(2);
        getDiscines.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<List<String>> data = new ArrayList<>();
                List<String> elementToDelete = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    data.add((ArrayList)snapshot.getValue());
                }
                for (String element : data.get(0)){
                    if (!data.get(1).contains(element)){
                        elementToDelete.add(element);
                    }
                }
                if (elementToDelete.isEmpty()){
                    elementToDelete.add("No discines to delete");
                }
                AlertDialog dialog;
                CharSequence[] dataS = elementToDelete.toArray(new CharSequence[elementToDelete.size()]);
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("DISCINES TO DELETE").setItems(dataS, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                dialog = builder.create();
                dialog.show();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void showCurrentDiscines(){

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("DISCINES");
        Query getCurrentDiscines = myRef.orderByKey().limitToLast(1);
        getCurrentDiscines.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> data = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    data.addAll((ArrayList)snapshot.getValue());
                }
                AlertDialog dialog;
                CharSequence[] dataS = data.toArray(new CharSequence[data.size()]);
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("DISCINES").setItems(dataS, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog = builder.create();
                dialog.show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void uploadData(){
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
        if (listDiscinesToPush.isEmpty()){
            Toast.makeText(this, "No discines to upload", Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(this, "Uploading data...", Toast.LENGTH_SHORT).show();
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference("DISCINES").child(DateTime.now().toString(fmt));
            myRef.setValue(listDiscinesToPush).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(MainActivity.this, "Data uploaded", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}
