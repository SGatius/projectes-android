package com.sgatius.hortcacistellera.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sgatius.hortcacistellera.R;
import com.sgatius.hortcacistellera.models.Document;

import java.util.List;

public class DocumentsAdapter extends BaseAdapter {

    static class  ViewHolder{
        TextView name_file;
    }

    private List<Document> list_documents;
    private LayoutInflater inflater = null;

    public DocumentsAdapter(Context context, List<Document> data){
        this.list_documents = data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getCount() {
        return list_documents.size();
    }

    @Override
    public Document getItem(int position) {
        return list_documents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            convertView = inflater.inflate(R.layout.row, null);
            holder = new ViewHolder();
            holder.name_file = (TextView)convertView.findViewById(R.id.item);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.name_file.setText(getItem(position).getName());
        return convertView;
    }
}
