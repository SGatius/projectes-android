package com.sgatius.hortcacistellera.models;

public class Product {
    private String name;
    private double price_unity, price_kg;

    public Product(String name, double price_unity, double price_kg) {
        this.name = name;
        this.price_unity = price_unity;
        this.price_kg = price_kg;
    }

    public Product() {
        this.name = "";
        this.price_unity = 0;
        this.price_kg = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice_unity() {
        return price_unity;
    }

    public void setPrice_unity(double price_unity) {
        this.price_unity = price_unity;
    }

    public double getPrice_kg() {
        return price_kg;
    }

    public void setPrice_kg(double price_kg) {
        this.price_kg = price_kg;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price_unity=" + price_unity +
                ", price_kg=" + price_kg +
                '}';
    }
}
