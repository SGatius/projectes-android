package com.sgatius.hortcacistellera;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.AsyncTaskLoader;
import androidx.loader.content.Loader;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sgatius.hortcacistellera.models.Product;
import com.tom_roush.pdfbox.pdmodel.PDDocument;
import com.tom_roush.pdfbox.pdmodel.PDPage;
import com.tom_roush.pdfbox.pdmodel.PDPageContentStream;
import com.tom_roush.pdfbox.pdmodel.font.PDType1Font;
import com.tom_roush.pdfbox.text.PDFTextStripper;
import com.tom_roush.pdfbox.text.PDFTextStripperByArea;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


import cdflynn.android.library.checkview.CheckView;

public class SecondActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<String> {

    private File file;
    private List<Product> productList;

    private CheckView checkView;
    private RelativeLayout progress_layout, main_layout;
    private TextView name_product, price_product;
    private EditText quantity;
    private FloatingActionButton f_previous, f_next, f_share;
    private int position = 0;
    private Map<String, Double> map_order;
    private boolean isFirst = true;

    public static final int APP_LOADER = 11;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        TextView info_text = findViewById(R.id.info_text);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.animacion);
        info_text.startAnimation(animation);
        progress_layout = findViewById(R.id.layout_progress);
        main_layout = findViewById(R.id.main_layout);
        checkView = findViewById(R.id.check);
        name_product = findViewById(R.id.name_product);
        price_product = findViewById(R.id.price_product);
        quantity = findViewById(R.id.quantity);
        f_previous = findViewById(R.id.btn_previous);
        f_next = findViewById(R.id.btn_next);
        f_share = findViewById(R.id.btn_share);

        productList = new ArrayList<>();
        map_order = new HashMap<>();

        String path = getIntent().getStringExtra("DOCUMENT_PATH");
        f_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMap();
                position -= 1;
                loadProductUI();
            }
        });

        f_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMap();
                position += 1;
                loadProductUI();
            }
        });

        f_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMap();
                createDocument();
            }
        });

        if (path == null){
            Toast.makeText(this, "Ha ocorregut agún error en la transferència del document.", Toast.LENGTH_SHORT).show();
        }else {
            Bundle bundle = new Bundle();
            bundle.putString("PATH", path);
            LoaderManager loader = getSupportLoaderManager();
            loader.initLoader(APP_LOADER, bundle, this);
            //getSupportLoaderManager().initLoader(APP_LOADER, bundle, this);

        }
    }

    private void createDocumentOrder(){
        try {
            String filename = Environment.getExternalStorageDirectory().getPath() + "/" + Environment.DIRECTORY_DOWNLOADS + "/Comanda Soco.pdf";
            PDDocument document = new PDDocument();
            PDPage page = new PDPage();
            document.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(document, page);
            contentStream.beginText();
            contentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
            contentStream.setLeading(14.5f);
            contentStream.newLineAtOffset(25, 725);
            for (int i = 0; i < productList.size(); i++){
                String key = productList.get(i).getName();
                if (map_order.containsKey(key)){
                    if (productList.get(i).getPrice_unity() > 0){
                        String text = productList.get(i).getName() + ": " + String.valueOf(map_order.get(productList.get(i).getName()) + " unitat/s");
                        contentStream.showText(text);
                    }else {
                        String text = productList.get(i).getName() + ": " + String.valueOf(map_order.get(productList.get(i).getName()) + " KG");
                        contentStream.showText(text);
                    }
                    contentStream.newLine();
                }
            }
            contentStream.endText();
            contentStream.close();
            document.save(filename);
            document.close();

        }catch (IOException e){
            if (e.getMessage() != null) {
                Log.e("WRITTING DOCUMENT ORDER", e.getMessage());
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void createDocumentTicket(){
        Calendar calendar = Calendar.getInstance();
        String date = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))
                + "-"
                + String.valueOf(calendar.get(Calendar.MONTH))
                + "-"
                + String.valueOf(calendar.get(Calendar.YEAR) - 1900);
        String filename = Environment.getExternalStorageDirectory().getPath()
                + "/"
                + Environment.DIRECTORY_DOWNLOADS
                + "/"
                + date
                + ".pdf";
        try {
            double total = 0;
            PDDocument document = new PDDocument();
            PDPage page = new PDPage();
            document.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(document, page);
            contentStream.beginText();
            contentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
            contentStream.setLeading(14.5f);
            contentStream.newLineAtOffset(25, 725);

            for (int i = 0; i < productList.size(); i++){
                String key = productList.get(i).getName();
                if (map_order.containsKey(key)){
                    double subtotal = 0;
                    String text = key + " Quantity: " + String.valueOf(map_order.get(key));
                    if (productList.get(i).getPrice_unity() > 0){
                        subtotal += (map_order.get(key)* productList.get(i).getPrice_unity());
                    }else {
                        subtotal += (map_order.get(key)* productList.get(i).getPrice_kg());
                    }
                    text = text + " SubTotal: " + String.valueOf(subtotal);
                    contentStream.showText(text);
                    contentStream.newLine();
                    total += subtotal;
                }
            }
            contentStream.showText("Total: " + String.valueOf(total) + " euros");

            contentStream.endText();
            contentStream.close();
            document.save(filename);
            document.close();
        }catch (IOException e){
            if (e.getMessage() != null){
                Log.e("DOCUMENT TICKET", e.getMessage());
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void createDocument(){
        checkView.uncheck();
        checkView.setVisibility(View.INVISIBLE);
        progress_layout.setVisibility(View.VISIBLE);
        createDocumentOrder();
        createDocumentTicket();
        progress_layout.setVisibility(View.GONE);
        checkView.setVisibility(View.VISIBLE);
        checkView.check();


    }

    private void loadMap(){
        if (!quantity.getText().toString().isEmpty()){
            map_order.put(productList.get(position).getName(), Double.parseDouble(quantity.getText().toString()));
        }
    }

    private void loadProductUI(){
        if (position == 0){
            f_previous.setVisibility(View.INVISIBLE);
        }
        if (position == 1){
            f_previous.setVisibility(View.VISIBLE);
        }
        if (position == productList.size() - 1){
            f_next.setVisibility(View.INVISIBLE);
            if (isFirst){
                isFirst = false;
                f_share.setVisibility(View.VISIBLE);
            }
        }
        if (position == productList.size() - 2){
            f_next.setVisibility(View.VISIBLE);
        }
        name_product.setText(productList.get(position).getName());
        if (productList.get(position).getPrice_unity() == 0){
            String text = String.valueOf(productList.get(position).getPrice_kg()) + " €/KG";
            price_product.setText(text);
        }else {
            String text = String.valueOf(productList.get(position).getPrice_unity()) + " €/Unitat";
            price_product.setText(text);
        }
        if (map_order.containsKey(productList.get(position).getName())){
            quantity.setText(String.valueOf(map_order.get(productList.get(position).getName())));
        }else {
            quantity.getText().clear();
        }
    }

    @NonNull
    @Override
    public Loader<String> onCreateLoader(int id, @Nullable final Bundle args) {
        return new AsyncTaskLoader<String>(this) {

            @Override
            protected void onStartLoading() {
                if (args != null){
                    progress_layout.setVisibility(View.VISIBLE);
                    forceLoad();
                }
            }

            @Nullable
            @Override
            public String loadInBackground() {
                String work = "";
                if (args != null) {
                    String path = args.getString("PATH");
                    work = getData(path);
                }
                return work;
            }
        };
    }

    @Override
    public void onLoadFinished(@NonNull Loader<String> loader, String data) {

        progress_layout.setVisibility(View.GONE);
        checkView.setVisibility(View.VISIBLE);
        checkView.check();
        main_layout.setVisibility(View.VISIBLE);
        if (data.equals(productList.toString())){
            Log.e("SEGON PLA", "S'ha carregat en segon pla");
            loadProductUI();
        }

    }

    @Override
    public void onLoaderReset(@NonNull Loader<String> loader) {

    }

    private String getData(String path){
        file = new File(path);

        try {
            PDDocument document = PDDocument.load(file);
            document.getClass();
            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition(true);
            PDFTextStripper tStripper = new PDFTextStripper();
            read(tStripper.getText(document));
            document.close();
        }catch (IOException e){
            if (e.getMessage() != null) {
                Log.e("PDFBOX", e.getMessage());
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        return productList.toString();
    }

    private void treatLines(String l_read){

        boolean isUnit = true;
        String name = "";
        double price_unit = 0;
        double price_kg = 0;
        Scanner word = new Scanner(l_read);
        while (word.hasNext()){
            String next_word = word.next();
            if (next_word.contains("€")){
                if (isUnit){
                    price_unit = Double.parseDouble(next_word.split("€")[0]);
                    isUnit = false;
                }else {
                    price_kg = Double.parseDouble(next_word.split("€")[0]);
                }
            }else if (next_word.equals("-")){
                if (isUnit){
                    isUnit = false;
                }
            }else {
                if (name.isEmpty()){
                    name = next_word;
                }else {
                    name = name + " " + next_word;
                }
            }

        }
        Product product = new Product(name, price_unit, price_kg);
        productList.add(product);
    }

    private void read(String str){
        Scanner scnLine = new Scanner(str);
        String line = "";
        int line_count = 0;
        while (scnLine.hasNextLine()){
            if (line_count < 2){
                line_count += 1;
                scnLine.nextLine();
            }else {
                line = scnLine.nextLine();
                if (!line.contains("Page")) {
                    if (!line.equals("Tots els preus porten l'IVA inclòs.")) {
                        if (!line.equals("Productes Preu unitats Preu quilo")) {
                            treatLines(line);
                        }
                    }
                }
            }
        }
    }
}
