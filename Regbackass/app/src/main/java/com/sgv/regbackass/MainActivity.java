package com.sgv.regbackass;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Transaction;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {


    private Button btnAssNoBreak, btnAssBreak, btnBackBreak;
    private NumberPicker numberPicker;
    private FirebaseFirestore db;
    double numAssNoBreak, numAssBreak;
    Map<String, Double> mapBackBreak;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = FirebaseFirestore.getInstance();

        btnAssNoBreak = findViewById(R.id.btn_sum_ass_no_break);
        btnAssBreak = findViewById(R.id.btn_sum_ass_break);
        numberPicker = findViewById(R.id.num_seat);
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(22);
        btnBackBreak = findViewById(R.id.btn_sum_back_break);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mapBackBreak = new HashMap<>();
                db.collection("Ass").document("NoBreak")
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if (task.isSuccessful()){
                                    numAssNoBreak = task.getResult().getDouble("number");
                                }
                            }
                        });
                db.collection("Ass").document("Break")
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if (task.isSuccessful()){
                                    numAssBreak = task.getResult().getDouble("number");
                                }
                            }
                        });
                db.collection("BackSeat").get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()){
                                    for (QueryDocumentSnapshot document : task.getResult()){
                                        mapBackBreak.put(document.getId(), document.getDouble("number"));
                                    }
                                }
                                sendMail();
                            }
                        });
            }
        });

        /*for (int i = 4; i < 23; i++){
            Map<String, Object> map = new HashMap<>();
            map.put("number", 0);
            db.collection("BackSeat").document(String.valueOf(i)).set(map);
        }*/

        btnAssNoBreak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DocumentReference reference = db.collection("Ass").document("NoBreak");
                db.runTransaction(new Transaction.Function<Double>() {
                    @Nullable
                    @Override
                    public Double apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                        DocumentSnapshot snapshot = transaction.get(reference);
                        double value = snapshot.getDouble("number") + 1;
                        transaction.update(reference, "number", value);
                        return value;
                    }
                }).addOnSuccessListener(new OnSuccessListener<Double>() {
                    @Override
                    public void onSuccess(Double aDouble) {
                        Toast.makeText(MainActivity.this, "Culs amb desgast: " + aDouble, Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(MainActivity.this, "Error en la transacció.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });



        btnAssBreak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DocumentReference reference = db.collection("Ass").document("Break");
                db.runTransaction(new Transaction.Function<Double>() {
                    @Nullable
                    @Override
                    public Double apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                        DocumentSnapshot snapshot = transaction.get(reference);
                        double value = snapshot.getDouble("number") + 1;
                        transaction.update(reference, "number", value);
                        return value;
                    }
                }).addOnSuccessListener(new OnSuccessListener<Double>() {
                    @Override
                    public void onSuccess(Double aDouble) {
                        Toast.makeText(MainActivity.this, "Culs totalment desgastats: " + aDouble, Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(MainActivity.this, "Error en la transacció.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        btnBackBreak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DocumentReference reference = db.collection("BackSeat").document(String.valueOf(numberPicker.getValue()));
                db.runTransaction(new Transaction.Function<Double>() {
                    @Nullable
                    @Override
                    public Double apply(@NonNull Transaction transaction) throws FirebaseFirestoreException {
                        DocumentSnapshot snapshot = transaction.get(reference);
                        double value = snapshot.getDouble("number") + 1;
                        transaction.update(reference, "number", value);
                        return value;
                    }
                }).addOnSuccessListener(new OnSuccessListener<Double>() {
                    @Override
                    public void onSuccess(Double aDouble) {
                        Toast.makeText(MainActivity.this, "Respaldo "+numberPicker.getValue() +": " + aDouble , Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private void sendMail(){
        String[] TO = {"montse.cortada@triangle.es"};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");

        String msg = "Número de culs amb desgast: " + numAssNoBreak + "\n"
                + "Número de culs totalment desgastats: " + numAssBreak + "\n"
                + "Relació respaldos: "
                + mapBackBreak.toString();
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Recompte butaques cinema");
        emailIntent.putExtra(Intent.EXTRA_TEXT, msg);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this,
                    "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }
}
