package com.sgv.nomina.model;

public class GeneralSalaryRate {

    private double salarianualbrut12,
    preuhora12,
    salarianualbrut3,
    preuhora3,
    trienni,
    salaribasemensual12,
    salaribasemensual3,
    pagaextra12,
    pagaextra3,
    plussessiogolfa12,
    plussessiogolfa3,
    recarrecsgolfa12,
    recarrecsgolfa3;

    public GeneralSalaryRate(double salarianualbrut12, double preuhora12, double salarianualbrut3, double preuhora3, double trienni, double salaribasemensual12, double salaribasemensual3, double pagaextra12, double pagaextra3, double plussessiogolfa12, double plussessiogolfa3, double recarrecsgolfa12, double recarrecsgolfa3) {
        this.salarianualbrut12 = salarianualbrut12;
        this.preuhora12 = preuhora12;
        this.salarianualbrut3 = salarianualbrut3;
        this.preuhora3 = preuhora3;
        this.trienni = trienni;
        this.salaribasemensual12 = salaribasemensual12;
        this.salaribasemensual3 = salaribasemensual3;
        this.pagaextra12 = pagaextra12;
        this.pagaextra3 = pagaextra3;
        this.plussessiogolfa12 = plussessiogolfa12;
        this.plussessiogolfa3 = plussessiogolfa3;
        this.recarrecsgolfa12 = recarrecsgolfa12;
        this.recarrecsgolfa3 = recarrecsgolfa3;
    }

    public GeneralSalaryRate(){

    }

    public double getSalarianualbrut12() {
        return salarianualbrut12;
    }

    public void setSalarianualbrut12(double salarianualbrut12) {
        this.salarianualbrut12 = salarianualbrut12;
    }

    public double getPreuhora12() {
        return preuhora12;
    }

    public void setPreuhora12(double preuhora12) {
        this.preuhora12 = preuhora12;
    }

    public double getSalarianualbrut3() {
        return salarianualbrut3;
    }

    public void setSalarianualbrut3(double salarianualbrut3) {
        this.salarianualbrut3 = salarianualbrut3;
    }

    public double getPreuhora3() {
        return preuhora3;
    }

    public void setPreuhora3(double preuhora3) {
        this.preuhora3 = preuhora3;
    }

    public double getTrienni() {
        return trienni;
    }

    public void setTrienni(double trienni) {
        this.trienni = trienni;
    }

    public double getSalaribasemensual12() {
        return salaribasemensual12;
    }

    public void setSalaribasemensual12(double salaribasemensual12) {
        this.salaribasemensual12 = salaribasemensual12;
    }

    public double getSalaribasemensual3() {
        return salaribasemensual3;
    }

    public void setSalaribasemensual3(double salaribasemensual3) {
        this.salaribasemensual3 = salaribasemensual3;
    }

    public double getPagaextra12() {
        return pagaextra12;
    }

    public void setPagaextra12(double pagaextra12) {
        this.pagaextra12 = pagaextra12;
    }

    public double getPagaextra3() {
        return pagaextra3;
    }

    public void setPagaextra3(double pagaextra3) {
        this.pagaextra3 = pagaextra3;
    }

    public double getPlussessiogolfa12() {
        return plussessiogolfa12;
    }

    public void setPlussessiogolfa12(double plussessiogolfa12) {
        this.plussessiogolfa12 = plussessiogolfa12;
    }

    public double getPlussessiogolfa3() {
        return plussessiogolfa3;
    }

    public void setPlussessiogolfa3(double plussessiogolfa3) {
        this.plussessiogolfa3 = plussessiogolfa3;
    }

    public double getRecarrecsgolfa12() {
        return recarrecsgolfa12;
    }

    public void setRecarrecsgolfa12(double recarrecsgolfa12) {
        this.recarrecsgolfa12 = recarrecsgolfa12;
    }

    public double getRecarrecsgolfa3() {
        return recarrecsgolfa3;
    }

    public void setRecarrecsgolfa3(double recarrecsgolfa3) {
        this.recarrecsgolfa3 = recarrecsgolfa3;
    }
}
