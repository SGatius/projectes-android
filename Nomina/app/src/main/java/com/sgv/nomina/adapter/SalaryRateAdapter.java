package com.sgv.nomina.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.view.View;
import android.widget.TextView;

import com.sgv.nomina.R;
import com.sgv.nomina.model.SalaryRateList;

import java.util.ArrayList;

public class SalaryRateAdapter extends ArrayAdapter<SalaryRateList> implements View.OnClickListener {

    private ArrayList<SalaryRateList> dataSet;

    Context mContext;

    private static class ViewHolder{
        TextView name, value;
    }

    public SalaryRateAdapter(ArrayList<SalaryRateList> data, Context context){
        super(context, R.layout.row_salary, data);
        this.dataSet = data;
        this.mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        SalaryRateList salary = getItem(position);
        ViewHolder viewHolder;
        final View result;

        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_salary, parent, false);
            viewHolder.name = (TextView)convertView.findViewById(R.id.text_name_salary);
            viewHolder.value = (TextView)convertView.findViewById(R.id.text_value_salary);
            result = convertView;
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
            result = convertView;
        }

        viewHolder.name.setText(salary.getName());
        viewHolder.value.setText(String.valueOf(salary.getValue()));
        return convertView;
    }

    @Override
    public void onClick(View v) {

    }
}
