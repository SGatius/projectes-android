package com.sgv.nomina.model;

import java.util.HashMap;
import java.util.Map;

public class TimeForWeek {

    private Map<Integer, Double> hoursPerDay;
    private Map<Integer, Boolean> golfaPerDay;
    private int plussessiogolfa;
    private int complementsessiogolfa;
    private int numberHolidays, numberPublicHolidays;

    public TimeForWeek(){
        hoursPerDay = new HashMap<>();
        golfaPerDay = new HashMap<>();
        plussessiogolfa = 0;
        complementsessiogolfa = 0;
        numberHolidays = 0;
        numberPublicHolidays = 0;
    }

    public void addTimeDay(int day, double hours){
        hoursPerDay.put(day,hours);
    }

    public void addGolfaDay(int day, boolean golfa){
        golfaPerDay.put(day, golfa);
    }

    public double getNumHoursOfWeek(){
        double hours = 0.0;
        for (int i = 0; i < hoursPerDay.size(); i++){
            if (golfaPerDay.get(i)){
                if (hoursPerDay.get(i) > 8){
                    plussessiogolfa += 1;
                }else {
                    complementsessiogolfa += 1;
                }
            }
            hours = hours + hoursPerDay.get(i);
        }
        return hours;
    }

    public int getPlussessiogolfa(){
        return plussessiogolfa;
    }

    public int getComplementsessiogolfa(){
        return complementsessiogolfa;
    }

    public int getNumberHolidays() {
        return numberHolidays;
    }

    public int getNumberPublicHolidays() {
        return numberPublicHolidays;
    }

    public void setNumberHolidays(int numberHolidays) {
        this.numberHolidays = numberHolidays;
    }

    public void setNumberPublicHolidays(int numberPublicHolidays) {
        this.numberPublicHolidays = numberPublicHolidays;
    }
}
