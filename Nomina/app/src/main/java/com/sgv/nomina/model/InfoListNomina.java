package com.sgv.nomina.model;

public class InfoListNomina {

    private String data;
    private double value;

    public InfoListNomina(String data, double value) {
        this.data = data;
        this.value = value;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
