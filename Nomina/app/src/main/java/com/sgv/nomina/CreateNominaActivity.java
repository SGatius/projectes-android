package com.sgv.nomina;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.sgv.nomina.model.GeneralSalaryRate;
import com.sgv.nomina.model.TimeForWeek;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CreateNominaActivity extends AppCompatActivity {

    private int numSetmanes = 1;

    private List<TimeForWeek> dataByWeek;

    private EditText hinici1m, hinici2m, hfinal1m, hfinal2m,
            hinici1t, hinici2t, hfinal1t, hfinal2t,
            hinici1w, hinici2w, hfinal1w, hfinal2w,
            hinici1th, hinici2th, hfinal1th, hfinal2th,
            hinici1f, hinici2f, hfinal1f, hfinal2f,
            hinici1s, hinici2s, hfinal1s, hfinal2s,
            hinici1sn, hinici2sn, hfinal1sn, hfinal2sn, numFestius, numVacances;

    private Switch golfam, golfat, golfaw, golfath, golfaf, golfas, golfasn;

    private TextView textNameWeek;

    private GeneralSalaryRate data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_nomina);

        loadReferences();
        data = new GeneralSalaryRate();
        getDataSalary();

        dataByWeek = new ArrayList<>();

        Button btnAddWeek = findViewById(R.id.btn_add_week);
        btnAddWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numSetmanes += 1;
                textNameWeek.setText("WEEK " + numSetmanes);
                cleanFields();
            }
        });

        Button btnLoadWeek = findViewById(R.id.btn_load_week);

        btnLoadWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimeForWeek timeForWeek = new TimeForWeek();
                //Dilluns
                if (!hinici1m.getText().toString().isEmpty()){
                    if (hfinal1m.getText().toString().isEmpty()){
                        Toast.makeText(CreateNominaActivity.this, "Any field isn't complete", Toast.LENGTH_SHORT).show();
                    }else {
                        //Controlem si hi han dos horaris o no
                        if (hinici2m.getText().toString().isEmpty()){
                            //No es dobla
                            String[] strHourInitM = hinici1m.getText().toString().split(":");
                            double hourInit = Double.valueOf(strHourInitM[0]);
                            hourInit += Double.valueOf(strHourInitM[1])/60;
                            String[] strHourFinalM = hfinal1m.getText().toString().split(":");
                            double hourFinal = Double.valueOf(strHourFinalM[0]);
                            hourFinal += Double.valueOf(strHourFinalM[1])/60;
                            timeForWeek.addTimeDay(0, hourFinal - hourInit);
                        }else {
                            //Es dobla per la raó que sigui
                            String[] strHourInitM1 = hinici1m.getText().toString().split(":");
                            String[] strHourInitM2 = hinici2m.getText().toString().split(":");
                            String[] strHourFinalM1 = hfinal1m.getText().toString().split(":");
                            String[] strHourFinalM2 = hfinal2m.getText().toString().split(":");
                            double time = Double.valueOf(strHourFinalM1[0]) + (Double.valueOf(strHourFinalM1[1])/60);
                            time = time - (Double.valueOf(strHourInitM1[0]) + (Double.valueOf(strHourInitM1[1])/60));
                            time = time + Double.valueOf(strHourFinalM2[0]) + (Double.valueOf(strHourFinalM2[1])/60);
                            time = time - (Double.valueOf(strHourInitM2[0]) + (Double.valueOf(strHourInitM2[1])/60));
                            timeForWeek.addTimeDay(0, time);
                        }
                        if (golfam.isChecked()){
                            timeForWeek.addGolfaDay(0, true);
                        }else {
                            timeForWeek.addGolfaDay(0,false);
                        }
                    }
                }else {
                    timeForWeek.addTimeDay(0,0);
                }

                //Dimarts

                if (!hinici1t.getText().toString().isEmpty()){
                    if (hfinal1t.getText().toString().isEmpty()){
                        Toast.makeText(CreateNominaActivity.this, "Any field isn't complete", Toast.LENGTH_SHORT).show();
                    }else {
                        //Controlem si hi han dos horaris o no
                        if (hinici2t.getText().toString().isEmpty()){
                            //No es dobla
                            String[] strHourInit = hinici1t.getText().toString().split(":");
                            double hourInit = Double.valueOf(strHourInit[0]);
                            hourInit += Double.valueOf(strHourInit[1])/60;
                            String[] strHourFinal = hfinal1t.getText().toString().split(":");
                            double hourFinal = Double.valueOf(strHourFinal[0]);
                            hourFinal += Double.valueOf(strHourFinal[1])/60;
                            timeForWeek.addTimeDay(1, hourFinal - hourInit);
                        }else {
                            //Es dobla per la raó que sigui
                            String[] strHourInit1 = hinici1t.getText().toString().split(":");
                            String[] strHourInit2 = hinici2t.getText().toString().split(":");
                            String[] strHourFinal1 = hfinal1t.getText().toString().split(":");
                            String[] strHourFinal2 = hfinal2t.getText().toString().split(":");
                            double time = Double.valueOf(strHourFinal1[0]) + (Double.valueOf(strHourFinal1[1])/60);
                            time = time - (Double.valueOf(strHourInit1[0]) + (Double.valueOf(strHourInit1[1])/60));
                            time = time + Double.valueOf(strHourFinal2[0]) + (Double.valueOf(strHourFinal2[1])/60);
                            time = time - (Double.valueOf(strHourInit2[0]) + (Double.valueOf(strHourInit2[1])/60));
                            timeForWeek.addTimeDay(1, time);
                        }
                        if (golfat.isChecked()){
                            timeForWeek.addGolfaDay(1, true);
                        }else {
                            timeForWeek.addGolfaDay(1,false);
                        }
                    }
                }else {
                    timeForWeek.addTimeDay(1,0);
                }

                //Dimecres

                if (!hinici1w.getText().toString().isEmpty()){
                    if (hfinal1w.getText().toString().isEmpty()){
                        Toast.makeText(CreateNominaActivity.this, "Any field isn't complete", Toast.LENGTH_SHORT).show();
                    }else {
                        //Controlem si hi han dos horaris o no
                        if (hinici2w.getText().toString().isEmpty()){
                            //No es dobla
                            String[] strHourInit = hinici1w.getText().toString().split(":");
                            double hourInit = Double.valueOf(strHourInit[0]);
                            hourInit += Double.valueOf(strHourInit[1])/60;
                            String[] strHourFinal = hfinal1w.getText().toString().split(":");
                            double hourFinal = Double.valueOf(strHourFinal[0]);
                            hourFinal += Double.valueOf(strHourFinal[1])/60;
                            timeForWeek.addTimeDay(2, hourFinal - hourInit);
                        }else {
                            //Es dobla per la raó que sigui
                            String[] strHourInit1 = hinici1w.getText().toString().split(":");
                            String[] strHourInit2 = hinici2w.getText().toString().split(":");
                            String[] strHourFinal1 = hfinal1w.getText().toString().split(":");
                            String[] strHourFinal2 = hfinal2w.getText().toString().split(":");
                            double time = Double.valueOf(strHourFinal1[0]) + (Double.valueOf(strHourFinal1[1])/60);
                            time = time - (Double.valueOf(strHourInit1[0]) + (Double.valueOf(strHourInit1[1])/60));
                            time = time + Double.valueOf(strHourFinal2[0]) + (Double.valueOf(strHourFinal2[1])/60);
                            time = time - (Double.valueOf(strHourInit2[0]) + (Double.valueOf(strHourInit2[1])/60));
                            timeForWeek.addTimeDay(2, time);
                        }
                        if (golfaw.isChecked()){
                            timeForWeek.addGolfaDay(2, true);
                        }else {
                            timeForWeek.addGolfaDay(2,false);
                        }
                    }
                }else {
                    timeForWeek.addTimeDay(2,0);
                }

                //Dijous

                if (!hinici1th.getText().toString().isEmpty()){
                    if (hfinal1th.getText().toString().isEmpty()){
                        Toast.makeText(CreateNominaActivity.this, "Any field isn't complete", Toast.LENGTH_SHORT).show();
                    }else {
                        //Controlem si hi han dos horaris o no
                        if (hinici2th.getText().toString().isEmpty()){
                            //No es dobla
                            String[] strHourInit = hinici1th.getText().toString().split(":");
                            double hourInit = Double.valueOf(strHourInit[0]);
                            hourInit += Double.valueOf(strHourInit[1])/60;
                            String[] strHourFinal = hfinal1th.getText().toString().split(":");
                            double hourFinal = Double.valueOf(strHourFinal[0]);
                            hourFinal += Double.valueOf(strHourFinal[1])/60;
                            timeForWeek.addTimeDay(3, hourFinal - hourInit);
                        }else {
                            //Es dobla per la raó que sigui
                            String[] strHourInit1 = hinici1th.getText().toString().split(":");
                            String[] strHourInit2 = hinici2th.getText().toString().split(":");
                            String[] strHourFinal1 = hfinal1th.getText().toString().split(":");
                            String[] strHourFinal2 = hfinal2th.getText().toString().split(":");
                            double time = Double.valueOf(strHourFinal1[0]) + (Double.valueOf(strHourFinal1[1])/60);
                            time = time - (Double.valueOf(strHourInit1[0]) + (Double.valueOf(strHourInit1[1])/60));
                            time = time + Double.valueOf(strHourFinal2[0]) + (Double.valueOf(strHourFinal2[1])/60);
                            time = time - (Double.valueOf(strHourInit2[0]) + (Double.valueOf(strHourInit2[1])/60));
                            timeForWeek.addTimeDay(3, time);
                        }
                        if (golfath.isChecked()){
                            timeForWeek.addGolfaDay(3, true);
                        }else {
                            timeForWeek.addGolfaDay(3,false);
                        }
                    }
                }else {
                    timeForWeek.addTimeDay(3, 0);
                }

                //Divendres

                if (!hinici1f.getText().toString().isEmpty()){
                    if (hfinal1f.getText().toString().isEmpty()){
                        Toast.makeText(CreateNominaActivity.this, "Any field isn't complete", Toast.LENGTH_SHORT).show();
                    }else {
                        //Controlem si hi han dos horaris o no
                        if (hinici2f.getText().toString().isEmpty()){
                            //No es dobla
                            String[] strHourInit = hinici1f.getText().toString().split(":");
                            double hourInit = Double.valueOf(strHourInit[0]);
                            hourInit += Double.valueOf(strHourInit[1])/60;
                            String[] strHourFinal = hfinal1f.getText().toString().split(":");
                            double hourFinal = Double.valueOf(strHourFinal[0]);
                            hourFinal += Double.valueOf(strHourFinal[1])/60;
                            timeForWeek.addTimeDay(4, hourFinal - hourInit);
                        }else {
                            //Es dobla per la raó que sigui
                            String[] strHourInit1 = hinici1f.getText().toString().split(":");
                            String[] strHourInit2 = hinici2f.getText().toString().split(":");
                            String[] strHourFinal1 = hfinal1f.getText().toString().split(":");
                            String[] strHourFinal2 = hfinal2f.getText().toString().split(":");
                            double time = Double.valueOf(strHourFinal1[0]) + (Double.valueOf(strHourFinal1[1])/60);
                            time = time - (Double.valueOf(strHourInit1[0]) + (Double.valueOf(strHourInit1[1])/60));
                            time = time + Double.valueOf(strHourFinal2[0]) + (Double.valueOf(strHourFinal2[1])/60);
                            time = time - (Double.valueOf(strHourInit2[0]) + (Double.valueOf(strHourInit2[1])/60));
                            timeForWeek.addTimeDay(4, time);
                        }
                        if (golfaf.isChecked()){
                            timeForWeek.addGolfaDay(4, true);
                        }else {
                            timeForWeek.addGolfaDay(4,false);
                        }
                    }
                }else {
                    timeForWeek.addTimeDay(4, 0);
                }

                //Dissabte

                if (!hinici1s.getText().toString().isEmpty()){
                    if (hfinal1s.getText().toString().isEmpty()){
                        Toast.makeText(CreateNominaActivity.this, "Any field isn't complete", Toast.LENGTH_SHORT).show();
                    }else {
                        //Controlem si hi han dos horaris o no
                        if (hinici2s.getText().toString().isEmpty()){
                            //No es dobla
                            String[] strHourInit = hinici1s.getText().toString().split(":");
                            double hourInit = Double.valueOf(strHourInit[0]);
                            hourInit += Double.valueOf(strHourInit[1])/60;
                            String[] strHourFinal = hfinal1s.getText().toString().split(":");
                            double hourFinal = Double.valueOf(strHourFinal[0]);
                            hourFinal += Double.valueOf(strHourFinal[1])/60;
                            timeForWeek.addTimeDay(5, hourFinal - hourInit);
                        }else {
                            //Es dobla per la raó que sigui
                            String[] strHourInit1 = hinici1s.getText().toString().split(":");
                            String[] strHourInit2 = hinici2s.getText().toString().split(":");
                            String[] strHourFinal1 = hfinal1s.getText().toString().split(":");
                            String[] strHourFinal2 = hfinal2s.getText().toString().split(":");
                            double time = Double.valueOf(strHourFinal1[0]) + (Double.valueOf(strHourFinal1[1])/60);
                            time = time - (Double.valueOf(strHourInit1[0]) + (Double.valueOf(strHourInit1[1])/60));
                            time = time + Double.valueOf(strHourFinal2[0]) + (Double.valueOf(strHourFinal2[1])/60);
                            time = time - (Double.valueOf(strHourInit2[0]) + (Double.valueOf(strHourInit2[1])/60));
                            timeForWeek.addTimeDay(5, time);
                        }
                        if (golfas.isChecked()){
                            timeForWeek.addGolfaDay(5, true);
                        }else {
                            timeForWeek.addGolfaDay(5,false);
                        }
                    }
                }else {
                    timeForWeek.addTimeDay(5,0);
                }

                //Diumenge

                if (!hinici1sn.getText().toString().isEmpty()){
                    if (hfinal1sn.getText().toString().isEmpty()){
                        Toast.makeText(CreateNominaActivity.this, "Any field isn't complete", Toast.LENGTH_SHORT).show();
                    }else {
                        //Controlem si hi han dos horaris o no
                        if (hinici2sn.getText().toString().isEmpty()){
                            //No es dobla
                            String[] strHourInit = hinici1sn.getText().toString().split(":");
                            double hourInit = Double.valueOf(strHourInit[0]);
                            hourInit += Double.valueOf(strHourInit[1])/60;
                            String[] strHourFinal = hfinal1sn.getText().toString().split(":");
                            double hourFinal = Double.valueOf(strHourFinal[0]);
                            hourFinal += Double.valueOf(strHourFinal[1])/60;
                            timeForWeek.addTimeDay(6, hourFinal - hourInit);
                        }else {
                            //Es dobla per la raó que sigui
                            String[] strHourInit1 = hinici1sn.getText().toString().split(":");
                            String[] strHourInit2 = hinici2sn.getText().toString().split(":");
                            String[] strHourFinal1 = hfinal1sn.getText().toString().split(":");
                            String[] strHourFinal2 = hfinal2sn.getText().toString().split(":");
                            double time = Double.valueOf(strHourFinal1[0]) + (Double.valueOf(strHourFinal1[1])/60);
                            time = time - (Double.valueOf(strHourInit1[0]) + (Double.valueOf(strHourInit1[1])/60));
                            time = time + Double.valueOf(strHourFinal2[0]) + (Double.valueOf(strHourFinal2[1])/60);
                            time = time - (Double.valueOf(strHourInit2[0]) + (Double.valueOf(strHourInit2[1])/60));
                            timeForWeek.addTimeDay(6, time);
                        }
                        if (golfasn.isChecked()){
                            timeForWeek.addGolfaDay(6, true);
                        }else {
                            timeForWeek.addGolfaDay(6,false);
                        }
                    }
                }else {
                    timeForWeek.addTimeDay(6,0);
                }

                if (!numFestius.getText().toString().isEmpty()){
                    timeForWeek.setNumberPublicHolidays(Integer.parseInt(numFestius.getText().toString()));
                }
                if (!numVacances.getText().toString().isEmpty()){
                    timeForWeek.setNumberHolidays(Integer.parseInt(numVacances.getText().toString()));
                }

                dataByWeek.add(timeForWeek);
            }
        });
        FloatingActionButton fab = findViewById(R.id.fab_calc_nomina);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcNomina();
            }
        });
    }

    private void calcNomina(){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        //Usarem la variable data per obtenir la informació de la taula salarial
        double salariBase = 0.0;
        double prorrataPagaExtra = 0.0;
        double trienni = 0.0;
        double horesComplementaries = 0.0;
        double plusSessionsGolfes = 0.0;
        double plusFestius = 0.0;
        double complementSessioGolfa = 0.0;
        double totaldevengado = 0.0;
        double contingenciascomunes = 0.0;
        double desempleo = 0.0;
        double formacionProfesional = 0.0;
        double retencion = 0.0;
        double totalaportaciones = 0.0;
        double irpf = Double.parseDouble(preferences.getString("irpf", ""));;
        int horescontracte = Integer.parseInt(preferences.getString("hoursContract", ""));
        int numerotriennis = Integer.parseInt(preferences.getString("numTrienni", ""));
        double numHoresExtra = getHoresExtra(horescontracte);

        double totaladeducir = 0.0;

        String typeEmployee = preferences.getString("typeEmployee", "");

        double liquidototalapercibir = 0.0;

        if (typeEmployee.equals("Cap de zona")){
            salariBase = (data.getSalaribasemensual3() * horescontracte)/40;
            prorrataPagaExtra = (data.getPagaextra3() * horescontracte)/40;
            horesComplementaries = numHoresExtra * data.getPreuhora3();
            for (TimeForWeek time : dataByWeek){
                time.getNumHoursOfWeek();
                plusSessionsGolfes += time.getPlussessiogolfa() * data.getPlussessiogolfa3();
                complementSessioGolfa += time.getComplementsessiogolfa() * data.getRecarrecsgolfa3();
                plusFestius += time.getNumberPublicHolidays() * 1.5 * data.getPreuhora3();
            }
        }else {
            salariBase = (data.getSalaribasemensual12() * horescontracte)/40;
            prorrataPagaExtra = (data.getPagaextra12() * horescontracte)/40;
            horesComplementaries = numHoresExtra * data.getPreuhora12();
            for (TimeForWeek time : dataByWeek){
                time.getNumHoursOfWeek();
                plusSessionsGolfes += time.getPlussessiogolfa() * data.getPlussessiogolfa12();
                complementSessioGolfa += time.getComplementsessiogolfa() * data.getRecarrecsgolfa12();
                plusFestius += time.getNumberPublicHolidays() * 1.5 * data.getPreuhora12();
            }
        }
        trienni = ((data.getTrienni() * numerotriennis)*horescontracte)/40;

        totaldevengado = salariBase
                + prorrataPagaExtra
                + trienni
                + horesComplementaries
                + plusSessionsGolfes
                + plusFestius
                + complementSessioGolfa;

        contingenciascomunes = totaldevengado * 0.047;
        desempleo = totaldevengado * 0.0155;
        formacionProfesional = totaldevengado * 0.001;
        retencion = totaldevengado * (irpf/100);

        totalaportaciones = contingenciascomunes
                + desempleo
                + formacionProfesional;

        totaladeducir = totalaportaciones + retencion;

        liquidototalapercibir = totaldevengado - totaladeducir;

        Toast.makeText(this, String.valueOf(liquidototalapercibir), Toast.LENGTH_LONG).show();


    }

    private double getHoresExtra(int horescontracte) {

        double extra = 0.0;
        for (TimeForWeek timeWeek : dataByWeek){
            double time = timeWeek.getNumHoursOfWeek();
            double hourcontract = horescontracte - (horescontracte* timeWeek.getNumberPublicHolidays()/6) - (horescontracte * timeWeek.getNumberHolidays()/6);
            extra += Math.abs (time - hourcontract - 2 * (timeWeek.getComplementsessiogolfa() + timeWeek.getPlussessiogolfa()));
        }
        return extra;
    }

    private void getDataSalary() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("infosalary")
                .document("salary")
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()){
                            data.setSalarianualbrut12(
                                    task.getResult().getDouble("salarianualbrut12"));
                            data.setPreuhora12(
                                    task.getResult().getDouble("preuhora12"));
                            data.setSalarianualbrut3(
                                    task.getResult().getDouble("salarianualbrut3"));
                            data.setPreuhora3(
                                    task.getResult().getDouble("preuhora3"));
                            data.setTrienni(
                                    task.getResult().getDouble("trienni"));
                            data.setSalaribasemensual12(
                                    task.getResult().getDouble("salaribasem12"));
                            data.setSalaribasemensual3(
                                    task.getResult().getDouble("salaribasem3"));
                            data.setPagaextra12(
                                    task.getResult().getDouble("pagaextra12"));
                            data.setPagaextra3(
                                    task.getResult().getDouble("pagaextra3"));
                            data.setPlussessiogolfa12(
                                    task.getResult().getDouble("plusgolfa12"));
                            data.setPlussessiogolfa3(
                                    task.getResult().getDouble("plusgolfa3"));
                            data.setRecarrecsgolfa12(
                                    task.getResult().getDouble("recarrecgolfa12"));
                            data.setRecarrecsgolfa3(
                                    task.getResult().getDouble("recarrecgolfa3"));



                        }
                    }
                });
    }

    private void cleanFields(){
        hinici1m.setText("");
        hinici2m.setText("");
        hfinal1m.setText("");
        hfinal2m.setText("");
        golfam.setChecked(false);

        hinici1t.setText("");
        hinici2t.setText("");
        hfinal1t.setText("");
        hfinal2t.setText("");
        golfat.setChecked(false);

        hinici1w.setText("");
        hinici2w.setText("");
        hfinal1w.setText("");
        hfinal2w.setText("");
        golfaw.setChecked(false);

        hinici1th.setText("");
        hinici2th.setText("");
        hfinal1th.setText("");
        hfinal2th.setText("");
        golfath.setChecked(false);

        hinici1f.setText("");
        hinici2f.setText("");
        hfinal1f.setText("");
        hfinal2f.setText("");
        golfaf.setChecked(false);

        hinici1s.setText("");
        hinici2s.setText("");
        hfinal1s.setText("");
        hfinal2s.setText("");
        golfas.setChecked(false);

        hinici1sn.setText("");
        hinici2sn.setText("");
        hfinal1sn.setText("");
        hfinal2sn.setText("");
        golfasn.setChecked(false);
    }

    private void loadReferences(){
        textNameWeek = findViewById(R.id.text_view_week);

        hinici1m = findViewById(R.id.hinicim1);
        hinici2m = findViewById(R.id.hinicim2);
        hfinal1m = findViewById(R.id.hfinalm1);
        hfinal2m = findViewById(R.id.hfinalm2);
        golfam = findViewById(R.id.ngolfam);
        //ngolfam = findViewById(R.id.ngolfam);

        hinici1t = findViewById(R.id.hinicit1);
        hinici2t = findViewById(R.id.hinicit2);
        hfinal1t = findViewById(R.id.hfinalt1);
        hfinal2t = findViewById(R.id.hfinalt2);
        golfat = findViewById(R.id.ngolfat);

        hinici1w = findViewById(R.id.hiniciw1);
        hinici2w = findViewById(R.id.hiniciw2);
        hfinal1w = findViewById(R.id.hfinalw1);
        hfinal2w = findViewById(R.id.hfinalw2);
        golfaw = findViewById(R.id.ngolfaw);

        hinici1th = findViewById(R.id.hinicith1);
        hinici2th = findViewById(R.id.hinicith2);
        hfinal1th = findViewById(R.id.hfinalth1);
        hfinal2th = findViewById(R.id.hfinalth2);
        golfath = findViewById(R.id.ngolfath);

        hinici1f = findViewById(R.id.hinicif1);
        hinici2f = findViewById(R.id.hinicif2);
        hfinal1f = findViewById(R.id.hfinalf1);
        hfinal2f = findViewById(R.id.hfinalf2);
        golfaf = findViewById(R.id.ngolfaf);

        hinici1s = findViewById(R.id.hinicis1);
        hinici2s = findViewById(R.id.hinicis2);
        hfinal1s = findViewById(R.id.hfinals1);
        hfinal2s = findViewById(R.id.hfinals2);
        golfas = findViewById(R.id.ngolfas);

        hinici1sn = findViewById(R.id.hinicisn1);
        hinici2sn = findViewById(R.id.hinicisn2);
        hfinal1sn = findViewById(R.id.hfinalsn1);
        hfinal2sn = findViewById(R.id.hfinalsn2);
        golfasn = findViewById(R.id.ngolfasn);

        numFestius = findViewById(R.id.num_festius);
        numVacances = findViewById(R.id.num_vacances);

    }
}
