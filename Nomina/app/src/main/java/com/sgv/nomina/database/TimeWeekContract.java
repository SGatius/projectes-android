package com.sgv.nomina.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public final class TimeWeekContract{

    private TimeWeekContract(){}

    public static class TimeWeek implements BaseColumns{
        public static final String TABLE_NAME = "tabledataweek";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_MES = "mes";
        public static final String COLUMN_NAME_SETMES = "set_mes";
        public static final String COLUMN_NAME_NUMHORES = "num_hores";
        public static final String COLUMN_NAME_NUMHORESCOMPL = "num_hores_compl";
        public static final String COLUMN_NAME_PLUSSGOLFA = "plus_s_golfa";
        public static final String COLUMN_NAME_COMPLSGOLFA = "compl_s_golfa";
        public static final String COLUMN_NAME_HFESTIUS = "h_festius";
        public static final String COLUMN_NAME_DFESTIUS = "d_festius";
        public static final String COLUMN_NAME_DVACANCES = "d_vacances";
    }

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TimeWeek.TABLE_NAME + " (" +
                    TimeWeek.COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    TimeWeek.COLUMN_NAME_MES + " TEXT, " +
                    TimeWeek.COLUMN_NAME_SETMES + " INTEGER, " +
                    TimeWeek.COLUMN_NAME_NUMHORES + " REAL, " +
                    TimeWeek.COLUMN_NAME_NUMHORESCOMPL + " REAL, " +
                    TimeWeek.COLUMN_NAME_PLUSSGOLFA + " INTEGER, " +
                    TimeWeek.COLUMN_NAME_COMPLSGOLFA + " INTEGER, " +
                    TimeWeek.COLUMN_NAME_HFESTIUS + " REAL, " +
                    TimeWeek.COLUMN_NAME_DFESTIUS + " INTEGER, " +
                    TimeWeek.COLUMN_NAME_DVACANCES + " INTEGER)";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + TimeWeek.TABLE_NAME;


    public class TimeWeekDBHelper extends SQLiteOpenHelper {

        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "TimeWeek.db";

        public TimeWeekDBHelper(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db){
            db.execSQL(SQL_CREATE_ENTRIES);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
            db.execSQL(SQL_DELETE_ENTRIES);
            onCreate(db);
        }

        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
            onUpgrade(db, oldVersion, newVersion);
        }

    }

}
