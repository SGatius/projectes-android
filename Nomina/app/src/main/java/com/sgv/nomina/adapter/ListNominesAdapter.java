package com.sgv.nomina.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sgv.nomina.R;
import com.sgv.nomina.model.InfoListNomina;
import com.sgv.nomina.model.SalaryRateList;

import java.util.ArrayList;

public class ListNominesAdapter extends ArrayAdapter<InfoListNomina> {

    private ArrayList<InfoListNomina> dataSet;
    private Context mContext;

    private static class ViewHolder{
        TextView data, value;
    }

    public ListNominesAdapter(ArrayList<InfoListNomina> data, Context context){
        super(this);
        this.dataSet = data;
        this.mContext = context;
    }

    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        InfoListNomina infoNomina = getItem(position);
        SalaryRateAdapter.ViewHolder viewHolder;
        final View result;

        if (convertView == null){
            viewHolder = new SalaryRateAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_salary, parent, false);
            viewHolder.da = (TextView)convertView.findViewById(R.id.text_name_salary);
            viewHolder.value = (TextView)convertView.findViewById(R.id.text_value_salary);
            result = convertView;
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (SalaryRateAdapter.ViewHolder)convertView.getTag();
            result = convertView;
        }

        viewHolder.name.setText(salary.getName());
        viewHolder.value.setText(String.valueOf(salary.getValue()));
        return convertView;
    }

}
