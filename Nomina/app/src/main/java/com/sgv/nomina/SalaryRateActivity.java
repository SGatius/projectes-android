package com.sgv.nomina;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.sgv.nomina.adapter.SalaryRateAdapter;
import com.sgv.nomina.model.SalaryRateList;

import java.util.ArrayList;

public class SalaryRateActivity extends AppCompatActivity {

    private ListView listView;
    private ArrayList<SalaryRateList> data;
    private SalaryRateAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salary_rate);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Salary rate");
        setSupportActionBar(toolbar);

        listView = findViewById(R.id.list_salary_rate);
        data = new ArrayList<>();
        adapter = new SalaryRateAdapter(data, getApplicationContext());
        listView.setAdapter(adapter);

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("infosalary")
                .document("salary")
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()){
                            data.add(new SalaryRateList("Salari anual brut (1,2)",
                                    task.getResult().getDouble("salarianualbrut12")));
                            data.add(new SalaryRateList("Preu/Hora(1,2)",
                                    task.getResult().getDouble("preuhora12")));
                            data.add(new SalaryRateList("Salari anual brut (3)",
                                    task.getResult().getDouble("salarianualbrut3")));
                            data.add(new SalaryRateList("Preu/Hora(3)",
                                    task.getResult().getDouble("preuhora3")));
                            data.add(new SalaryRateList("Trienni",
                                    task.getResult().getDouble("trienni")));
                            data.add(new SalaryRateList("Salari base mensual(1,2)",
                                    task.getResult().getDouble("salaribasem12")));
                            data.add(new SalaryRateList("Salari base mensual(3)",
                                    task.getResult().getDouble("salaribasem3")));
                            data.add(new SalaryRateList("Paga extra(1,2)",
                                    task.getResult().getDouble("pagaextra12")));
                            data.add(new SalaryRateList("Paga extra(3)",
                                    task.getResult().getDouble("pagaextra3")));
                            data.add(new SalaryRateList("Plus sessió golfa(1,2)",
                                    task.getResult().getDouble("plusgolfa12")));
                            data.add(new SalaryRateList("Plus sessió golfa(3)",
                                    task.getResult().getDouble("plusgolfa3")));
                            data.add(new SalaryRateList("Recàrrec golfa(1,2)",
                                    task.getResult().getDouble("recarrecgolfa12")));
                            data.add(new SalaryRateList("Recarrec golfa(3)",
                                    task.getResult().getDouble("recarrecgolfa3")));

                            adapter.notifyDataSetChanged();

                        }
                    }
                });
    }
}
