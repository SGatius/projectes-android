package com.sgv.nomina.model;

public class SalaryRateList {

    private String name;
    private double value;

    public SalaryRateList(String name, double value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }
}
