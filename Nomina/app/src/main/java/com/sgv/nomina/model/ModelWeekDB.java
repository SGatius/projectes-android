package com.sgv.nomina.model;

public class ModelWeekDB {

    private int id, setMes, plusSGolfa, complSGolfa, dFestius, dVacances;
    private double numHores, numHoresCompl, hFestius;
    private String nomMes;

    public ModelWeekDB() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSetMes() {
        return setMes;
    }

    public void setSetMes(int setMes) {
        this.setMes = setMes;
    }

    public int getPlusSGolfa() {
        return plusSGolfa;
    }

    public void setPlusSGolfa(int plusSGolfa) {
        this.plusSGolfa = plusSGolfa;
    }

    public int getComplSGolfa() {
        return complSGolfa;
    }

    public void setComplSGolfa(int complSGolfa) {
        this.complSGolfa = complSGolfa;
    }

    public int getdFestius() {
        return dFestius;
    }

    public void setdFestius(int dFestius) {
        this.dFestius = dFestius;
    }

    public int getdVacances() {
        return dVacances;
    }

    public void setdVacances(int dVacances) {
        this.dVacances = dVacances;
    }

    public double getNumHores() {
        return numHores;
    }

    public void setNumHores(double numHores) {
        this.numHores = numHores;
    }

    public double getNumHoresCompl() {
        return numHoresCompl;
    }

    public void setNumHoresCompl(double numHoresCompl) {
        this.numHoresCompl = numHoresCompl;
    }

    public double gethFestius() {
        return hFestius;
    }

    public void sethFestius(double hFestius) {
        this.hFestius = hFestius;
    }

    public String getNomMes() {
        return nomMes;
    }

    public void setNomMes(String nomMes) {
        this.nomMes = nomMes;
    }
}
