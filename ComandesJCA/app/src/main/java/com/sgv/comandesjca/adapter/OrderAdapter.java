package com.sgv.comandesjca.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sgv.comandesjca.R;
import com.sgv.comandesjca.model.Order;

import java.util.ArrayList;
import java.util.List;

public class OrderAdapter extends BaseAdapter {

    static class ViewHolder{
        TextView textProduct;
        EditText edittextQuantity;
    }

    private List<Order> listProducts;

    private LayoutInflater inflater = null;

    public OrderAdapter(Context context, List<Order> data){
        inflater = LayoutInflater.from(context);
        this.listProducts = data;
    }



    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getCount() {
        return listProducts.size();
    }

    @Override
    public Order getItem(int position) {
        return listProducts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            convertView = inflater.inflate(R.layout.row_item, null);
            holder = new ViewHolder();
            holder.textProduct = (TextView) convertView.findViewById(R.id.text_product);
            holder.edittextQuantity = (EditText)convertView.findViewById(R.id.num_quantity);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        String name = getItem(position).getProduct();
        holder.textProduct.setText(name);
        if (!getItem(position).getQuantity().isEmpty()){
            holder.edittextQuantity.setText(getItem(position).getQuantity());
        }



        return convertView;
    }

    public List<Order> getListProducts(){
        return listProducts;
    }

}
