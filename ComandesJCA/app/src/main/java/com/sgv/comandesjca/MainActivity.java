package com.sgv.comandesjca;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.independentsoft.office.odf.Spreadsheet;
import com.independentsoft.office.odf.Table;
import com.sgv.comandesjca.model.Order;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_STORAGE = 7193;
    private List<Order> orderList;
    private TextView title, tProduct;
    private EditText etQuantity;
    private FloatingActionButton fabPrevious;
    private FloatingActionButton fabNext;
    private int pos = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

        orderList = new ArrayList<>();
        loadListProducts();

        title = findViewById(R.id.title_theme_product);
        tProduct = findViewById(R.id.name_product);
        etQuantity = findViewById(R.id.quantity);

        title.setText(getTitleTheme());
        tProduct.setText(orderList.get(pos).getProduct());
        etQuantity.setText(orderList.get(pos).getQuantity());

        FloatingActionButton fab = findViewById(R.id.fab_save_send_order);
        fabPrevious = findViewById(R.id.fab_previous);
        fabNext = findViewById(R.id.fab_next);


        fabPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String quantity = etQuantity.getText().toString();
                if (quantity.isEmpty()){
                    quantity = "0";
                }
                orderList.get(pos).setQuantity(quantity);
                pos -= 1;
                fabNext.show();
                title.setText(getTitleTheme());
                tProduct.setText(orderList.get(pos).getProduct());
                etQuantity.setText(orderList.get(pos).getQuantity());
                if (pos == 0){
                    fabPrevious.hide();
                }
            }
        });

        fabNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String quantity = etQuantity.getText().toString();
                if (quantity.isEmpty()){
                    quantity = "0";
                }
                orderList.get(pos).setQuantity(quantity);
                pos += 1;
                fabPrevious.show();
                title.setText(getTitleTheme());
                tProduct.setText(orderList.get(pos).getProduct());
                etQuantity.setText(orderList.get(pos).getQuantity());
                if (pos + 1 == orderList.size()){
                    fabNext.hide();
                }
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos == orderList.size() - 1){
                    if (etQuantity.getText().toString().isEmpty()){
                        orderList.get(pos).setQuantity("0");
                    }else {
                        orderList.get(pos).setQuantity(etQuantity.getText().toString());
                    }
                }
                writeOrderAndSend();
            }
        });
    }

    private String getTitleTheme(){
        if (pos >= 0 && pos <= 8){
            return "Cobega PET";
        }else if (pos >= 9 && pos <= 24){
            return "Cobega Bag In Box";
        }else if (pos >= 25 && pos <= 27){
            return "Capri Sun";
        }else if (pos >= 28 && pos <= 29){
            return "CINE";
        }else if (pos >= 30 && pos <= 34){
            return "Vimbodi";
        }else if (pos >= 35 && pos <= 36){
            return "Honest Tea";
        }else if (pos >= 37 && pos <= 41){
            return "Tork";
        }else if (pos >= 42 && pos <= 69){
            return "Albert Soler";
        }else if (pos >= 70 && pos <= 79){
            return "TEKIND";
        }else if (pos >= 80 && pos <= 83){
            return "ADES";
        }else if (pos >= 84 && pos <= 98){
            return "Congelats Borrás";
        }else if (pos >= 99 && pos <= 106){
            return "Botiquín Asepeyo";
        }else if (pos >= 107 && pos <= 109){
            return "Caixes Crispetes";
        }else if (pos == 110){
            return "Zaforsa";
        }
        else {
            return "Taquilla";
        }
    }

    private void writeOrderAndSend(){
        boolean allChecked = true;
        //writeToFile();
        for (int i = 0; i < orderList.size(); i++){
            if (orderList.get(i).getQuantity().isEmpty()){
                allChecked = false;
            }
        }
        if (!allChecked){
            Toast.makeText(this, "There is a product that not checked yet", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Write to file", Toast.LENGTH_SHORT).show();
            writeToFile();
        }
    }

    private void writeToFile(){
        String fileToRead = Environment.getExternalStorageDirectory().getPath() + "/" + Environment.DIRECTORY_DOWNLOADS + "/COMANDA GENERAL.ods";

        try {
            Spreadsheet spreadsheet = new Spreadsheet(fileToRead);


            List<Table> sheets = spreadsheet.getTables();
            modifyInventory(sheets.get(0));
            DateTime dateTime = DateTime.now();
            DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM.yyyy");
            String nameFile = "COMANDA GENERAL " + dateTime.toString(fmt);
            String fileToSave = Environment.getExternalStorageDirectory().getPath()
                    + "/"
                    + Environment.DIRECTORY_DOWNLOADS
                    +"/"
                    + nameFile
                    + ".ods";
            spreadsheet.save(fileToSave);
            sendToMail(fileToSave, nameFile);

        }catch (Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            Log.e("Spreedsheet: ", e.getMessage());
        }

        ///storage/self/primary/Download/COMANDA GENERAL.ods
    }

    private void sendToMail(String dirFile, String nameFile){
        File file = new File(dirFile);
        Uri uri = FileProvider.getUriForFile(this, "com.sgv.comandesjca.FileProvider", file);
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, nameFile);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"montse.cortada@triangle.es", "sandra.aguila@triangle.es"});
        //emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"sgatius93@gmail.com"});
        emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
        emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        try {
            startActivity(Intent.createChooser(emailIntent, "Enviar email..."));
            finish();
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this,
                    "No tienes clientes de email instalados.", Toast.LENGTH_SHORT).show();
            Log.e("SEND MAIL", "L'usuari no té client de mail");
        }
    }

    private void modifyInventory(Table t){
        //For testing
        /*for (int i = 0; i < orderList.size(); i++){
            orderList.get(i).setQuantity(String.valueOf(i));
        }*/
        //COBEGA PET
        t.get("C4").setValue(orderList.get(0).getQuantity());
        t.get("C5").setValue(orderList.get(1).getQuantity());
        t.get("C6").setValue(orderList.get(2).getQuantity());
        t.get("C7").setValue(orderList.get(3).getQuantity());
        t.get("C8").setValue(orderList.get(4).getQuantity());
        t.get("C9").setValue(orderList.get(5).getQuantity());
        t.get("C10").setValue(orderList.get(6).getQuantity());
        t.get("C11").setValue(orderList.get(7).getQuantity());
        t.get("C12").setValue(orderList.get(8).getQuantity());
        //COBEGA BAG IN BOX
        t.get("C15").setValue(orderList.get(9).getQuantity());
        t.get("C16").setValue(orderList.get(10).getQuantity());
        t.get("C17").setValue(orderList.get(11).getQuantity());
        t.get("C18").setValue(orderList.get(12).getQuantity());
        t.get("C19").setValue(orderList.get(13).getQuantity());
        t.get("C20").setValue(orderList.get(14).getQuantity());
        t.get("C21").setValue(orderList.get(15).getQuantity());
        t.get("C22").setValue(orderList.get(16).getQuantity());
        t.get("C23").setValue(orderList.get(17).getQuantity());
        t.get("C24").setValue(orderList.get(18).getQuantity());
        t.get("C25").setValue(orderList.get(19).getQuantity());
        t.get("C26").setValue(orderList.get(20).getQuantity());
        t.get("C27").setValue(orderList.get(21).getQuantity());
        t.get("C28").setValue(orderList.get(22).getQuantity());
        t.get("C29").setValue(orderList.get(23).getQuantity());
        t.get("C30").setValue(orderList.get(24).getQuantity());
        //CAPRI SUN
        t.get("C33").setValue(orderList.get(25).getQuantity());
        t.get("C34").setValue(orderList.get(26).getQuantity());
        t.get("C35").setValue(orderList.get(27).getQuantity());
        //CINE
        t.get("C53").setValue(orderList.get(28).getQuantity());
        t.get("C54").setValue(orderList.get(29).getQuantity());
        //VIMBODI
        t.get("E3").setValue(orderList.get(30).getQuantity());
        t.get("E4").setValue(orderList.get(31).getQuantity());
        t.get("E5").setValue(orderList.get(32).getQuantity());
        t.get("E6").setValue(orderList.get(33).getQuantity());
        t.get("E7").setValue(orderList.get(34).getQuantity());
        //HONEST TEA
        t.get("E9").setValue(orderList.get(35).getQuantity());
        t.get("E10").setValue(orderList.get(36).getQuantity());
        //TORK
        t.get("E15").setValue(orderList.get(37).getQuantity());
        t.get("E16").setValue(orderList.get(38).getQuantity());
        t.get("E17").setValue(orderList.get(39).getQuantity());
        t.get("E18").setValue(orderList.get(40).getQuantity());
        t.get("E19").setValue(orderList.get(41).getQuantity());
        //ALBERT SOLER
        t.get("E21").setValue(orderList.get(42).getQuantity());
        t.get("E22").setValue(orderList.get(43).getQuantity());
        t.get("E23").setValue(orderList.get(44).getQuantity());
        t.get("E24").setValue(orderList.get(45).getQuantity());
        t.get("E25").setValue(orderList.get(46).getQuantity());
        t.get("E26").setValue(orderList.get(47).getQuantity());
        t.get("E27").setValue(orderList.get(48).getQuantity());
        t.get("E28").setValue(orderList.get(49).getQuantity());
        t.get("E29").setValue(orderList.get(50).getQuantity());
        t.get("E30").setValue(orderList.get(51).getQuantity());
        t.get("E31").setValue(orderList.get(52).getQuantity());
        t.get("E32").setValue(orderList.get(53).getQuantity());
        t.get("E33").setValue(orderList.get(54).getQuantity());
        t.get("E34").setValue(orderList.get(55).getQuantity());
        t.get("E35").setValue(orderList.get(56).getQuantity());
        t.get("E36").setValue(orderList.get(57).getQuantity());
        t.get("E37").setValue(orderList.get(58).getQuantity());
        t.get("E38").setValue(orderList.get(59).getQuantity());
        t.get("E39").setValue(orderList.get(60).getQuantity());
        t.get("E40").setValue(orderList.get(61).getQuantity());
        t.get("E41").setValue(orderList.get(62).getQuantity());
        t.get("E42").setValue(orderList.get(63).getQuantity());
        t.get("E43").setValue(orderList.get(64).getQuantity());
        t.get("E44").setValue(orderList.get(65).getQuantity());
        t.get("E45").setValue(orderList.get(66).getQuantity());
        t.get("E46").setValue(orderList.get(67).getQuantity());
        t.get("E47").setValue(orderList.get(68).getQuantity());
        t.get("E48").setValue(orderList.get(69).getQuantity());
        //TEKIND
        t.get("E50").setValue(orderList.get(70).getQuantity());
        t.get("E51").setValue(orderList.get(71).getQuantity());
        t.get("E52").setValue(orderList.get(72).getQuantity());
        t.get("E53").setValue(orderList.get(73).getQuantity());
        t.get("E54").setValue(orderList.get(74).getQuantity());
        t.get("E55").setValue(orderList.get(75).getQuantity());
        t.get("E56").setValue(orderList.get(76).getQuantity());
        t.get("E57").setValue(orderList.get(77).getQuantity());
        t.get("E58").setValue(orderList.get(78).getQuantity());
        t.get("E59").setValue(orderList.get(79).getQuantity());
        //ADES
        t.get("G3").setValue(orderList.get(80).getQuantity());
        t.get("G4").setValue(orderList.get(81).getQuantity());
        t.get("G5").setValue(orderList.get(82).getQuantity());
        t.get("G6").setValue(orderList.get(83).getQuantity());
        //BORRAS
        t.get("G8").setValue(orderList.get(84).getQuantity());
        t.get("G9").setValue(orderList.get(85).getQuantity());
        t.get("G10").setValue(orderList.get(86).getQuantity());
        t.get("G11").setValue(orderList.get(87).getQuantity());
        t.get("G12").setValue(orderList.get(88).getQuantity());

        t.get("G16").setValue(orderList.get(89).getQuantity());
        t.get("G17").setValue(orderList.get(90).getQuantity());
        t.get("G18").setValue(orderList.get(91).getQuantity());
        t.get("G19").setValue(orderList.get(92).getQuantity());

        t.get("G22").setValue(orderList.get(93).getQuantity());
        t.get("G23").setValue(orderList.get(94).getQuantity());

        t.get("G25").setValue(orderList.get(95).getQuantity());
        t.get("G26").setValue(orderList.get(96).getQuantity());
        t.get("G27").setValue(orderList.get(97).getQuantity());

        t.get("G30").setValue(orderList.get(98).getQuantity());
        //Botiquin ASEPEYO
        t.get("G32").setValue(orderList.get(99).getQuantity());
        t.get("G33").setValue(orderList.get(100).getQuantity());
        t.get("G34").setValue(orderList.get(101).getQuantity());
        t.get("G35").setValue(orderList.get(102).getQuantity());
        t.get("G36").setValue(orderList.get(103).getQuantity());
        t.get("G37").setValue(orderList.get(104).getQuantity());
        t.get("G38").setValue(orderList.get(105).getQuantity());
        t.get("G39").setValue(orderList.get(106).getQuantity());
        //CAIXES CRISPETES
        t.get("G41").setValue(orderList.get(107).getQuantity());
        t.get("G42").setValue(orderList.get(108).getQuantity());
        t.get("G43").setValue(orderList.get(109).getQuantity());
        //ZAFORSA
        t.get("G45").setValue(orderList.get(110).getQuantity());
        //TAQUILLA
        t.get("G47").setValue(orderList.get(111).getQuantity());
        t.get("G48").setValue(orderList.get(112).getQuantity());
        t.get("G49").setValue(orderList.get(113).getQuantity());
        t.get("G50").setValue(orderList.get(114).getQuantity());
        t.get("G51").setValue(orderList.get(115).getQuantity());
        t.get("G52").setValue(orderList.get(116).getQuantity());
        t.get("G53").setValue(orderList.get(117).getQuantity());
        t.get("G54").setValue(orderList.get(118).getQuantity());
        t.get("G55").setValue(orderList.get(119).getQuantity());
        t.get("G56").setValue(orderList.get(120).getQuantity());
        t.get("G57").setValue(orderList.get(121).getQuantity());
        t.get("G58").setValue(orderList.get(122).getQuantity());
    }

    private void loadListProducts(){
        orderList.add(new Order("Cocacola (PET)", ""));
        orderList.add(new Order("Cocacola Zero (PET)", ""));
        orderList.add(new Order("Fanta Taronja (PET)", ""));
        orderList.add(new Order("Fanta Llimona (PET)", ""));
        orderList.add(new Order("Sprite (PET)", ""));
        orderList.add(new Order("Nestea (PET)", ""));
        orderList.add(new Order("Aquarius Llimona (PET)", ""));
        orderList.add(new Order("Aquarius Taronja (PET)", ""));
        orderList.add(new Order("Aigua", ""));
        orderList.add(new Order("Cocacola", ""));
        orderList.add(new Order("Cocacola Zero", ""));
        orderList.add(new Order("Cocacola Zero Zero", ""));
        orderList.add(new Order("Fanta taronja Zero", ""));
        orderList.add(new Order("fanta Llimona Zero", ""));
        orderList.add(new Order("Sprite", ""));
        orderList.add(new Order("Nestea", ""));
        orderList.add(new Order("Aquarius Zero", ""));
        orderList.add(new Order("Gas (Buides)", ""));
        orderList.add(new Order("Gas (Plenes)", ""));
        orderList.add(new Order("Caixa Gots 40cl", ""));
        orderList.add(new Order("Caixa Gots 50cl", ""));
        orderList.add(new Order("caixa Gots 75cl", ""));
        orderList.add(new Order("Tapes gots 40-50cl", ""));
        orderList.add(new Order("Tapes gots 75cl", ""));
        orderList.add(new Order("Canyes", ""));
        orderList.add(new Order("Capri Sun Orange", ""));
        orderList.add(new Order("Capri Sun Safari Fruits", ""));
        orderList.add(new Order("Capri Sun Multi Vitamin", ""));
        orderList.add(new Order("Ulleres 3D Noves", ""));
        orderList.add(new Order("Ulleres 3D reciclades", ""));
        orderList.add(new Order("Cacaolat 275ml", ""));
        orderList.add(new Order("Cacaolat Mocca 275ml", ""));
        orderList.add(new Order("Estrella Damm (Vermella)", ""));
        orderList.add(new Order("Block Damm (Negra)", ""));
        orderList.add(new Order("Voll Damm (Verda)", ""));
        orderList.add(new Order("Honest Tea Latte", ""));
        orderList.add(new Order("Honest Tea Capuccino", ""));
        orderList.add(new Order("Paper Water (Caixa 6 bobines)", ""));
        orderList.add(new Order("Paper assecamans verd (caixa 6 bobines)", ""));
        orderList.add(new Order("Paper netja blau (Caixa 6 bobines)", ""));
        orderList.add(new Order("Càrregues sabó (Caixa 6 unitats)", ""));
        orderList.add(new Order("Bosses brossa tork", ""));
        orderList.add(new Order("Bosses brossa (90x110)(18 rotllos)", ""));
        orderList.add(new Order("Bosses xuxes (20x30)(18 paquets)", ""));
        orderList.add(new Order("Pinces xuxes", ""));
        orderList.add(new Order("pales xuxes", ""));
        orderList.add(new Order("KH Prof. Baños (Verd)", ""));
        orderList.add(new Order("KH Prof. Multisuperfícies y cristales (Blau)", ""));
        orderList.add(new Order("KH Prof. Insecticida y fregasuelos (Lila)", ""));
        orderList.add(new Order("KH Prof. Quitagrasas (Blanc)", ""));
        orderList.add(new Order("Precinte", ""));
        orderList.add(new Order("Bayeta", ""));
        orderList.add(new Order("Estropajo", ""));
        orderList.add(new Order("Estropajo Alumini", ""));
        orderList.add(new Order("Brides", ""));
        orderList.add(new Order("Serrin", ""));
        orderList.add(new Order("Amoniac", ""));
        orderList.add(new Order("Lleixiu", ""));
        orderList.add(new Order("Salfuman", ""));
        orderList.add(new Order("Limpiador amoniacal", ""));
        orderList.add(new Order("Guants TB 718STAC", ""));
        orderList.add(new Order("Guants", ""));
        orderList.add(new Order("Bosses amb anses (20 paquets)", ""));
        orderList.add(new Order("Raspall escombra", ""));
        orderList.add(new Order("Pal escombra", ""));
        orderList.add(new Order("Recollidor", ""));
        orderList.add(new Order("Pal recollidor", ""));
        orderList.add(new Order("Mocho fregona", ""));
        orderList.add(new Order("Pols de marbre", ""));
        orderList.add(new Order("Peladilles de sal", ""));
        orderList.add(new Order("Blat Salat", ""));
        orderList.add(new Order("Blat Dolç", ""));
        orderList.add(new Order("Sal", ""));
        orderList.add(new Order("Sucre groc", ""));
        orderList.add(new Order("Sucre taronja", ""));
        orderList.add(new Order("Sucre blau", ""));
        orderList.add(new Order("Sucre vermell", ""));
        orderList.add(new Order("Sucre verd", ""));
        orderList.add(new Order("Oli", ""));
        orderList.add(new Order("Arena limpiaollas Tekind", ""));
        orderList.add(new Order("Almendra de leyenda", ""));
        orderList.add(new Order("Avena de la buena", ""));
        orderList.add(new Order("Coco genial", ""));
        orderList.add(new Order("Soja manzana", ""));
        orderList.add(new Order("Magnum Doble Xocolata", ""));
        orderList.add(new Order("Magnum Doble Caramel", ""));
        orderList.add(new Order("Magnum White", ""));
        orderList.add(new Order("Magnum Frac", ""));
        orderList.add(new Order("Magnum Almendrado", ""));
        orderList.add(new Order("Cornetto Vainilla", ""));
        orderList.add(new Order("Cornetto Nata", ""));
        orderList.add(new Order("Cornetto Xocolata", ""));
        orderList.add(new Order("Negritón", ""));
        orderList.add(new Order("Calippo Llima Llimona", ""));
        orderList.add(new Order("Calippo Llimona", ""));
        orderList.add(new Order("Ben & Jerry's Chocolate", ""));
        orderList.add(new Order("Ben & Jerry's Cheesecake", ""));
        orderList.add(new Order("Ben & Jerry's Cookies", ""));
        orderList.add(new Order("Sacs de gel 10KG", ""));
        orderList.add(new Order("Aigua Oxigenada", ""));
        orderList.add(new Order("Alcohol", ""));
        orderList.add(new Order("Povidona yodada", ""));
        orderList.add(new Order("Gases (Caixa)", ""));
        orderList.add(new Order("Suero Fisiològic (Caixa)", ""));
        orderList.add(new Order("Tirites (Caixa)", ""));
        orderList.add(new Order("Esparadrap", ""));
        orderList.add(new Order("Benes 7cm", ""));
        orderList.add(new Order("Caixa Crispetes 90g", ""));
        orderList.add(new Order("Caixa Crispetes 150g", ""));
        orderList.add(new Order("Caixa Crispetes 225g", ""));
        orderList.add(new Order("Paper entrades taquilla paquets 5000", ""));
        orderList.add(new Order("Gomes pollatre", ""));
        orderList.add(new Order("Paper registradora tèrmic(Datàfon)", ""));
        orderList.add(new Order("Paper 80x80 (Xuxes, ATM, taquilla)", ""));
        orderList.add(new Order("Ceys tack cristal (Celo de vidre)", ""));
        orderList.add(new Order("Bolis", ""));
        orderList.add(new Order("Blister 2€", ""));
        orderList.add(new Order("Blister 1€", ""));
        orderList.add(new Order("Blister 0,50€", ""));
        orderList.add(new Order("Blister 0,20€", ""));
        orderList.add(new Order("Blister 0,10€", ""));
        orderList.add(new Order("WC 6 desincrustante", ""));
        orderList.add(new Order("Paper DIN-A4", ""));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "To use this app you must accept this permissions", Toast.LENGTH_SHORT).show();
                    finishAffinity();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
}
