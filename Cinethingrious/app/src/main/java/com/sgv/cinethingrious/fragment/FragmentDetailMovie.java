package com.sgv.cinethingrious.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.sgv.cinethingrious.R;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentDetailMovie.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentDetailMovie#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentDetailMovie extends Fragment {

    private String id;
    private ArrayAdapter adapter;
    private List<String> data;
    private AlertDialog progressDialog;

    private OnFragmentInteractionListener mListener;

    public FragmentDetailMovie() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.

     * @return A new instance of fragment FragmentDetailMovie.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentDetailMovie newInstance(String id) {
        FragmentDetailMovie fragment = new FragmentDetailMovie();
        Bundle args = new Bundle();
        args.putString("ID", id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getString("ID");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_detail_movie, container, false);
        progressDialog = new SpotsDialog.Builder()
                .setContext(v.getContext())
                .setMessage(0)
                .setTheme(0)
                .setCancelable(false)
                .build();
        progressDialog.show();
        ListView listView = v.findViewById(R.id.list_detail_movies);
        data = new ArrayList<>();
        adapter = new ArrayAdapter(v.getContext(), android.R.layout.simple_list_item_1, data);
        listView.setAdapter(adapter);
        getData();
        return v;
    }

    private void getData(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("Movies Gone Away")
                .document(id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()){
                            data.addAll((List<String>)task.getResult().get("Movies"));
                            progressDialog.hide();
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
