package com.sgv.cinethingrious.activity;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.sgv.cinethingrious.R;
import com.sgv.cinethingrious.adapter.MoviesAdapter;
import com.sgv.cinethingrious.model.Movie;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;

public class HistoryMoviesActivity extends AppCompatActivity{

    private List<Movie> movieList;
    private MoviesAdapter adapter;
    private SearchView searchView;
    private AlertDialog progressDialogInfo;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_movies);

        progressDialogInfo = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Loading...")
                .setTheme(0)
                .setCancelable(false)
                .build();
        progressDialogInfo.show();
        listView = findViewById(R.id.list_history_movie);
        movieList = new ArrayList<>();
        adapter = new MoviesAdapter(this, movieList);
        listView.setAdapter(adapter);
        readMovies();
    }

    private void readMovies(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("MoviesHistory")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){
                            for (QueryDocumentSnapshot documentSnapshot : task.getResult()){
                                Movie movie = new Movie(documentSnapshot.get("title").toString(), documentSnapshot.get("date").toString());
                                movieList.add(movie);
                            }
                            progressDialogInfo.hide();
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                adapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id==R.id.action_search){
            //do your functionality here
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
