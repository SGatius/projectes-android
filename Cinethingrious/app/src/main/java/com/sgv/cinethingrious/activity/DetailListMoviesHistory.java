package com.sgv.cinethingrious.activity;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sgv.cinethingrious.R;
import com.sgv.cinethingrious.fragment.FragmentDetailMovie;

public class DetailListMoviesHistory extends AppCompatActivity implements FragmentDetailMovie.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_list_movies_history);
        Bundle arguments = new Bundle();
        String id = getIntent().getStringExtra("ID");
        arguments.putString("ID", id);
        FragmentDetailMovie fragment = FragmentDetailMovie.newInstance(id);
        fragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.detail_container, fragment)
                .commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
