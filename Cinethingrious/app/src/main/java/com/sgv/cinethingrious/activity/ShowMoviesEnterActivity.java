package com.sgv.cinethingrious.activity;

import android.app.AlertDialog;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.sgv.cinethingrious.R;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class ShowMoviesEnterActivity extends AppCompatActivity {

    private FirebaseFirestore db;
    private List<String> dataMovie;
    private ArrayAdapter adapter;
    private List<String> lastWeek, currentWeek;
    private AlertDialog progressDialogInfo;
    private String weekIsGoneAway;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_movies_enter);
        db = FirebaseFirestore.getInstance();
        progressDialogInfo = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Checking movies that enter")
                .setTheme(0)
                .setCancelable(false)
                .build();
        dataMovie = new ArrayList<>();
        weekIsGoneAway = "";
        ListView listView = findViewById(R.id.list_movies_enter);
        FloatingActionButton fab = findViewById(R.id.fab_upload_movies_enter);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialogInfo.setMessage("Uploading movies that enter this week");
                progressDialogInfo.show();
                Map<String, Object> data = new HashMap<>();
                data.put("ID", weekIsGoneAway);
                data.put("Movies", dataMovie);
                db.collection("Movies Enter")
                        .document(weekIsGoneAway)
                        .set(data)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                progressDialogInfo.hide();
                            }
                        });
            }
        });
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, dataMovie);
        listView.setAdapter(adapter);
        getDataMovie();
    }

    private DateTime setToFridayIfNotFriday(DateTime dateTime){
        int day = dateTime.getDayOfWeek();
        switch (day){
            case 1:
                dateTime = dateTime.plusDays(4);//numToFriday = 4;
                break;
            case 2:
                dateTime = dateTime.plusDays(3);//numToFriday = 3;
                break;
            case 3:
                dateTime = dateTime.plusDays(2); //numToFriday = 2;
                break;
            case 4:
                dateTime = dateTime.plusDays(1);//numToFriday = 1;
                break;
            case 5:
                //numToFriday = 0;
                break;
            case 6:
                dateTime = dateTime.minusDays(1);//numToFriday = -1;
                break;
            case 7:
                dateTime = dateTime.minusDays(2);//numToFriday = -2;
                break;
        }
        return dateTime;
    }

    private String getDateCurrentWeek(){
        DateTime dateTime = new DateTime();
        dateTime = setToFridayIfNotFriday(dateTime);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd-MM-yyyy");
        return dateTime.toString(fmt) + " | " + dateTime.plusDays(6).toString(fmt);
    }

    private String getDateLastWeek(){
        DateTime dateTime = new DateTime();
        dateTime = setToFridayIfNotFriday(dateTime);
        dateTime = dateTime.minusDays(1);
        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd-MM-yyyy");
        return dateTime.minusDays(6).toString(fmt) + " | " + dateTime.toString(fmt);
    }

    private void getDataMovie() {
        progressDialogInfo.show();
        lastWeek = new ArrayList<>();
        currentWeek = new ArrayList<>();
        final String dateCurrentWeek = getDateCurrentWeek();
        final String dateLastWeek = getDateLastWeek();

        db.collection("Movies")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){
                            for (QueryDocumentSnapshot documentSnapshot : task.getResult()){
                                Log.d("READING DATABASE", documentSnapshot.getId());
                                if (documentSnapshot.getId().equals(dateCurrentWeek)) {
                                    weekIsGoneAway = documentSnapshot.getId();
                                    currentWeek.addAll((List<String>) documentSnapshot.getData().get("Movies"));
                                }if (documentSnapshot.getId().equals(dateLastWeek)){
                                    lastWeek.addAll((List<String>) documentSnapshot.getData().get("Movies"));
                                }
                            }
                            for (String movie : currentWeek){
                                if (!lastWeek.contains(movie)){
                                    dataMovie.add(movie);
                                }
                            }
                            progressDialogInfo.hide();
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
    }

}
