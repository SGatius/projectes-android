package com.sgv.cinethingrious.activity;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sgv.cinethingrious.fragment.ListMoviesEnterFragment;
import com.sgv.cinethingrious.R;

public class ShowHistoryMoviesEnterActivity extends AppCompatActivity implements ListMoviesEnterFragment.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_history_movies_enter);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.list_container, ListMoviesEnterFragment.newInstance());
        ft.commit();
    }

    @Override
    public void onFragmentInteraction(String id) {
        Intent intent = new Intent(ShowHistoryMoviesEnterActivity.this, DetailHistoryMoviesEnter.class);
        intent.putExtra("ID", id);
        startActivity(intent);
    }
}
