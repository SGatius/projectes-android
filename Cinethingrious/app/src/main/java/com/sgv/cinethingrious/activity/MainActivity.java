package com.sgv.cinethingrious.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.sgv.cinethingrious.R;

public class MainActivity extends AppCompatActivity {

    private EditText emailText, passwordText;
    private Button loginButton;
    private ProgressBar progressBar;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        emailText = findViewById(R.id.input_email);
        passwordText = findViewById(R.id.input_password);
        loginButton = findViewById(R.id.btn_login);
        mAuth = FirebaseAuth.getInstance();

        progressBar = findViewById(R.id.progress_bar_login);

        emailText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(s.toString()).matches()){
                    emailText.setError("Enter a valid email address");
                }else {
                    emailText.setError(null);
                }
            }
        });
        passwordText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().isEmpty() || s.toString().length() < 6){
                    passwordText.setError("Password must be contains 6 alphanumeric charactaters at least ");
                }else {
                    passwordText.setError(null);
                }
            }
        });
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate()){
                    progressBar.setVisibility(View.VISIBLE);
                    mAuth.signInWithEmailAndPassword(emailText.getText().toString(), passwordText.getText().toString())
                            .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()){
                                        Log.d("LOGIN PROCESS", "Success");
                                        progressBar.setVisibility(View.GONE);
                                        startActivity(new Intent(MainActivity.this, FunctionsActivity.class));
                                    }else {
                                        Log.e("LOGINS PROCESS", task.getException().getMessage());
                                        progressBar.setVisibility(View.GONE);
                                        Toast.makeText(MainActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }

            }
        });
    }

    private boolean isValidate(){
        if (emailText.getText().toString().isEmpty()){
            emailText.setError("This field can't be empty");
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(emailText.getText().toString()).matches()){
            emailText.setError("Enter a valid email address");
            return false;
        }
        if (passwordText.getText().toString().isEmpty()){
            passwordText.setError("This field can't be empty");
            return false;
        }
        return true;
    }
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        if (mAuth.getCurrentUser() != null){
            startActivity(new Intent(MainActivity.this, FunctionsActivity.class));
        }
    }
}
