package com.sgv.cinethingrious.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.sgv.cinethingrious.R;

import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentDetailsHistoryMoviesEnter.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentDetailsHistoryMoviesEnter#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentDetailsHistoryMoviesEnter extends Fragment implements AdapterView.OnItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "ID";
    private ArrayAdapter adapter;
    private List<String> data;

    // TODO: Rename and change types of parameters
    private String id;
    private AlertDialog progressDialog;

    private OnFragmentInteractionListener mListener;

    public FragmentDetailsHistoryMoviesEnter() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id Date which movies was enter on billboard.
     * @return A new instance of fragment FragmentDetailsHistoryMoviesEnter.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentDetailsHistoryMoviesEnter newInstance(String id) {
        FragmentDetailsHistoryMoviesEnter fragment = new FragmentDetailsHistoryMoviesEnter();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_details_history_movies_enter, container, false);
        progressDialog = new SpotsDialog.Builder()
                .setContext(view.getContext())
                .setMessage("Loading...")
                .setTheme(0)
                .setCancelable(false)
                .build();
        progressDialog.show();
        data = new ArrayList<>();
        ListView listView = view.findViewById(R.id.list_movies_history_detail_enter);
        adapter = new ArrayAdapter(view.getContext(), android.R.layout.simple_list_item_1, data);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        loadData();

        return view;
    }

    private void loadData() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("Movies Enter")
                .document(id)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()){
                            data.addAll((List<String>)task.getResult().get("Movies"));
                            progressDialog.hide();
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        onButtonPressed(parent.getItemAtPosition(position).toString());
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String nameMovie) {
        if (mListener != null) {
            mListener.onFragmentInteraction(nameMovie);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String nameMovie);
    }
}
