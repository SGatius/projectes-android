package com.sgv.cinethingrious.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.sgv.cinethingrious.R;
import com.sgv.cinethingrious.fragment.FragmentDetailsHistoryMoviesEnter;

public class DetailHistoryMoviesEnter extends AppCompatActivity implements FragmentDetailsHistoryMoviesEnter.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_historiy_movies_enter);
        Bundle arguments = new Bundle();
        String id = getIntent().getStringExtra("ID");
        arguments.putString("ID", id);
        FragmentDetailsHistoryMoviesEnter fragment = FragmentDetailsHistoryMoviesEnter.newInstance(id);
        fragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.detail_container, fragment)
                .commit();
    }

    @Override
    public void onFragmentInteraction(String nameMovie) {
        Toast.makeText(this, nameMovie, Toast.LENGTH_SHORT).show();
    }
}
