package com.sgv.cinethingrious.activity;

import android.app.AlertDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.sgv.cinethingrious.R;
import com.sgv.cinethingrious.adapter.CandyAdapter;
import com.sgv.cinethingrious.model.Candy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class ListCandyShopActivity extends AppCompatActivity {

    private CandyAdapter adapter;
    private List<Candy> candies;
    private FirebaseFirestore db;
    private AlertDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_candy_shop);
        candies = new ArrayList<>();
        db = FirebaseFirestore.getInstance();
        ListView listView = findViewById(R.id.list_candy_shop);
        progressDialog = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage("Loading database")
                .setTheme(0)
                .setCancelable(false)
                .build();
        progressDialog.show();
        adapter = new CandyAdapter(this, candies);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Candy candy = (Candy)parent.getItemAtPosition(position);
                AlertDialog.Builder builder = new AlertDialog.Builder(ListCandyShopActivity.this)
                        .setTitle(candy.getName())
                        .setCancelable(true)
                        .setView(R.layout.custom_image_candy_dialog);
                AlertDialog dialog = builder.create();
                dialog.show();
                ImageView imageView = dialog.findViewById(R.id.image_candy);
                imageView.setImageResource(getImage(candy.getId()));

            }
        });
        //insertData();
        loadData();
    }

    private int getImage(int id) {
        int image = 0;
        switch (id){
            case 100010:
                image = R.mipmap.ic_imp_caseras;
                break;
            case 100413:
                image = R.mipmap.ic_chip_aperitivo;
                break;
            default:
                image = R.mipmap.ic_launcher_round;
                break;
        }
        return image;
    }

    private void insertData(){
        List<Candy> candies = new ArrayList<>();

        candies.add(Candy.newInstance(	100010	, "Imp Caseras"));
        candies.add(Candy.newInstance(	100413	,"Chips Premium Aperitivo"));
        candies.add(Candy.newInstance(	100913	,"Chips Premium Queso Cabra"));
        candies.add(Candy.newInstance(	101013	,"Chips Premium Oliva Anchoa"));
        candies.add(Candy.newInstance(	110311	,"Imp Ligeras (oregano + tomate)"));
        candies.add(Candy.newInstance(	120305	,"Chips Vinagre y sal"));
        candies.add(Candy.newInstance(	120712	,"Chips Mediterràneas"));
        candies.add(Candy.newInstance(	160112	,"Chips Onduladas jamon"));
        candies.add(Candy.newInstance(	200113	,"Coctel Snacks"));
        candies.add(Candy.newInstance(	201112	,"Cortezas de cerdo"));
        candies.add(Candy.newInstance(	201305	,"Chicharricos"));
        candies.add(Candy.newInstance(	220012	,"Tronkees"));
        candies.add(Candy.newInstance(	231013	,"Imp Triblis	"));
        candies.add(Candy.newInstance(	231513	,"Imp Texicos Tex-Mex"));
        candies.add(Candy.newInstance(	241113	,"Ruedas"));
        candies.add(Candy.newInstance(	241213	,"Ruedas Ketchup"));
        candies.add(Candy.newInstance(	250113	,"Fritos"	));
        candies.add(Candy.newInstance(	270013	,"Ganchitos"));
        candies.add(Candy.newInstance(	270706	,"Gusifrits mantequilla"));
        candies.add(Candy.newInstance(	291013	,"Bolas queso	"));
        candies.add(Candy.newInstance(	300225	,"Almendras Garrapiñadas"));
        candies.add(Candy.newInstance(	380129	,"Cacahuete"));
        candies.add(Candy.newInstance(	381125	,"Cacahuete garrapiñado"));
        candies.add(Candy.newInstance(	390129	,"Anacardos"));
        candies.add(Candy.newInstance(	501029	,"Panís gran"));
        candies.add(Candy.newInstance(	501229	,"Panís"));
        candies.add(Candy.newInstance(	503106	,"Palomitas Mantequilla"));
        candies.add(Candy.newInstance(	511329	,"Pipes pelades"));
        candies.add(Candy.newInstance(	541029	,"Coctel s/c"));
        candies.add(Candy.newInstance(	541229	,"Coctel fse picante"));
        candies.add(Candy.newInstance(	541729	,"Coctel chili"));
        candies.add(Candy.newInstance(	543029	,"Coctel fse dietetico"));
        candies.add(Candy.newInstance(	580029	,"Coctel oriental"));
        candies.add(Candy.newInstance(	590129	,"Coctel tropical"));
        candies.add(Candy.newInstance(	601404	,"Freson ac.gig."));
        candies.add(Candy.newInstance(	603301	,"Tradicional"));
        candies.add(Candy.newInstance(	603302	,"café"));
        candies.add(Candy.newInstance(	603303	,"Fresa nata"));
        candies.add(Candy.newInstance(	603305	,"Limon"));
        candies.add(Candy.newInstance(	603308	,"Menta nata"));
        candies.add(Candy.newInstance(	605089	,"Sidral pica candy"));
        candies.add(Candy.newInstance(	605104	,"Wherter's original"));
        candies.add(Candy.newInstance(	605911	,"Palotes surtido"));
        candies.add(Candy.newInstance(	607400	,"Natural"));
        candies.add(Candy.newInstance(	607401	,"Natural menta"));
        candies.add(Candy.newInstance(	611823	,"Chupachups"));
        candies.add(Candy.newInstance(	615002	,"Sugus"));
        candies.add(Candy.newInstance(	615100	,"Eucalip. Mentol"));
        candies.add(Candy.newInstance(	615101	,"Regaliz Mentol"));
        candies.add(Candy.newInstance(	615102	,"Mel Mentol"));
        candies.add(Candy.newInstance(	615103	,"Limón Mentol"));
        candies.add(Candy.newInstance(	615151	,"Sin azucar"));
        candies.add(Candy.newInstance(	628109	,"Cors xocolata"));
        candies.add(Candy.newInstance(	628111	,"Fresas rellenas"));
        candies.add(Candy.newInstance(	628114	,"Labios fresa rellenos"));
        candies.add(Candy.newInstance(	628117	,"Besos rellenos"));
        candies.add(Candy.newInstance(	628122	,"Konos twist rellenos"));
        candies.add(Candy.newInstance(	628123	,"Sesos rellenos"));
        candies.add(Candy.newInstance(	628124	,"Moras rellenas"));
        candies.add(Candy.newInstance(	628134	,"Dentadures"));
        candies.add(Candy.newInstance(	628142	,"Tortugas rellenas"));
        candies.add(Candy.newInstance(	628236	,"Bot cola rellena"));
        candies.add(Candy.newInstance(	628238	,"Fres.bosque rellena"));
        candies.add(Candy.newInstance(	628360	,"Maxi Crujitos"));
        candies.add(Candy.newInstance(	628381	,"Chocoladrillazos fresa"));
        candies.add(Candy.newInstance(	628545	,"Rayadito nata/fresa	"));
        candies.add(Candy.newInstance(	628563	,"Dulcipica melón"));
        candies.add(Candy.newInstance(	628564	,"Dulcipica manzana"));
        candies.add(Candy.newInstance(	628718	,"Ladrillo Pica Fresa"));
        candies.add(Candy.newInstance(	628719	,"Besos twist	"));
        candies.add(Candy.newInstance(	628721	,"Ladrillo relleno pica fresa"));
        candies.add(Candy.newInstance(	628740	,"Ladrillo Relleno Pica Cola"));
        candies.add(Candy.newInstance(	628742	,"Ladrillos fresa	"));
        candies.add(Candy.newInstance(	634090	,"Pikotas cereza"	));
        candies.add(Candy.newInstance(	634099	,"Gummy jelly	"));
        candies.add(Candy.newInstance(	634766	,"Anacondas"));
        candies.add(Candy.newInstance(	634767	,"Pulpos brillo"));
        candies.add(Candy.newInstance(	634770	,"Pulpos pica	"));
        candies.add(Candy.newInstance(	634804	,"Burguer"));
        candies.add(Candy.newInstance(	634857	,"Pizzes"));
        candies.add(Candy.newInstance(	634862	,"Miniburguer"));
        candies.add(Candy.newInstance(	634879	,"Ojo relleno"));
        candies.add(Candy.newInstance(	640516	,"Lenguas manzana"));
        candies.add(Candy.newInstance(	640800	,"Chupete goma"));
        candies.add(Candy.newInstance(	640900	,"Bipop"));
        candies.add(Candy.newInstance(	641001	,"Collares"));
        candies.add(Candy.newInstance(	642050	,"Mini fruit"));
        candies.add(Candy.newInstance(	643001	,"Fizz rolls"));
        candies.add(Candy.newInstance(	645025	,"Gajos"));
        candies.add(Candy.newInstance(	645053	,"Ovnis Pica-pica"));
        candies.add(Candy.newInstance(	645055	,"Frambuesas"));
        candies.add(Candy.newInstance(	645056	,"Platanos"));
        candies.add(Candy.newInstance(	645057	,"Setas"));
        candies.add(Candy.newInstance(	645058	,"Heladitos"));
        candies.add(Candy.newInstance(	645060	,"Melones	"));
        candies.add(Candy.newInstance(	647000	,"Nata-Fresa"));
        candies.add(Candy.newInstance(	647002	,"Nata-Mora"));
        candies.add(Candy.newInstance(	650102	,"Torcida Frambuesa"));
        candies.add(Candy.newInstance(	650132	,"Bolas golf fresa"));
        candies.add(Candy.newInstance(	650133	,"Finitronc twisti"));
        candies.add(Candy.newInstance(	650170	,"Guindillas picante"	));
        candies.add(Candy.newInstance(	650387	,"Jumbos fresa"));
        candies.add(Candy.newInstance(	650388	,"Jumbos pica fresa"));
        candies.add(Candy.newInstance(	650389	,"Jumbo torcida fresa"));
        candies.add(Candy.newInstance(	650390	,"Jumbo torcida relleno helado"));
        candies.add(Candy.newInstance(	650392	,"Jumbos sandia"));
        candies.add(Candy.newInstance(	650393	,"Lenguas fresa"));
        candies.add(Candy.newInstance(	650394	,"Relleno fresa"));
        candies.add(Candy.newInstance(	650395	,"Lenguas multifruit"));
        candies.add(Candy.newInstance(	650398	,"palo melocotón"));
        candies.add(Candy.newInstance(	650410	,"Maxi relleno pica fresa"));
        candies.add(Candy.newInstance(	650413	,"Jumbo Pica Tornado"));
        candies.add(Candy.newInstance(	650414	,"Jumbo Tornado"));
        candies.add(Candy.newInstance(	650417	,"Relleno cola"));
        candies.add(Candy.newInstance(	650418	,"Relleno pica cola"));
        candies.add(Candy.newInstance(	650420	,"Jumbo Pica Cola	"));
        candies.add(Candy.newInstance(	650423	,"Lenguas cola"));
        candies.add(Candy.newInstance(	650509	,"Finitronc cremosos"));
        candies.add(Candy.newInstance(	650600	,"Super finitronc	"));
        candies.add(Candy.newInstance(	650700	,"Tanzanitos"));
        candies.add(Candy.newInstance(	651110	,"Dedos pica"));
        candies.add(Candy.newInstance(	651128	,"Huevos gigantes"));
        candies.add(Candy.newInstance(	665220	,"Disco rojo"));
        candies.add(Candy.newInstance(	665221	,"Disco negro"));
        candies.add(Candy.newInstance(	665224	,"Ladrillo relleno (nata/fresa)"));
        candies.add(Candy.newInstance(	665300	,"Cartucheras"));
        candies.add(Candy.newInstance(	665330	,"Mega torcida Roja"));
        candies.add(Candy.newInstance(	665331	,"Mega torcida Negra"));
        candies.add(Candy.newInstance(	665381	,"Spagueti fresa"));
        candies.add(Candy.newInstance(	665470	,"Soft Kiss"));
        candies.add(Candy.newInstance(	665498	,"Jelly beans"));
        candies.add(Candy.newInstance(	665506	,"Mini Botella Cola Brillo"));
        candies.add(Candy.newInstance(	665507	,"Cereza azuc.sup	"));
        candies.add(Candy.newInstance(	665515	,"Maxicola"));
        candies.add(Candy.newInstance(	665518	,"Gusanos brillo"));
        candies.add(Candy.newInstance(	665519	,"Maxi cola pica"));
        candies.add(Candy.newInstance(	665574	,"Capsulas"));
        candies.add(Candy.newInstance(	665604	,"Lagrimas menta"));
        candies.add(Candy.newInstance(	665617	,"Tochos fresa (Refrescantes)"));
        candies.add(Candy.newInstance(	668282	,"Favorit Red&White (RAMBLA)"));
        candies.add(Candy.newInstance(	668284	,"Tubular Chamallow (RAMBLA)"));
        candies.add(Candy.newInstance(	668360	,"Mega Torcida Fresa (RAMBLA)"));
        candies.add(Candy.newInstance(	670010	,"Ositos de oro 125G.(24 u.)"));
        candies.add(Candy.newInstance(	671050	,"Osito brillo sin azúcar"));
        candies.add(Candy.newInstance(	678000	,"kilimanjaro	"));
        candies.add(Candy.newInstance(	678001	,"fizzy sAhara"));
        candies.add(Candy.newInstance(	678003	,"fizzy arizona"));
        candies.add(Candy.newInstance(	678004	,"fizzy caribe"));
        candies.add(Candy.newInstance(	678020	,"teide"));
        candies.add(Candy.newInstance(	678040	,"savanna mix"));
        candies.add(Candy.newInstance(	678041	,"amazonia mix"));
        candies.add(Candy.newInstance(	689072	,"Caña azúcar"));
        candies.add(Candy.newInstance(	689200	,"Cotton candy"));
        candies.add(Candy.newInstance(	689214	,"Marsmallow coco"));
        candies.add(Candy.newInstance(	691002	,"Monchitos"));
        candies.add(Candy.newInstance(	691206	,"Corazón Gelatina Fresa"));
        candies.add(Candy.newInstance(	691210	,"Calaveras Surtida"));
        candies.add(Candy.newInstance(	691347	,"Fruta italiana"	));
        candies.add(Candy.newInstance(	698004	,"Monedas fresa"));
        candies.add(Candy.newInstance(	698005	,"Monedas regaliz	"));
        candies.add(Candy.newInstance(	698012	,"Frutitas brillo"));
        candies.add(Candy.newInstance(	698140	,"Llaves fresa ácida"	));
        candies.add(Candy.newInstance(	698170	,"Batidos fresa"));
        candies.add(Candy.newInstance(	700071	,"Filipinos Blanc"));
        candies.add(Candy.newInstance(	700072	,"Filipinos Negre"));
        candies.add(Candy.newInstance(	700120	,"Toblerone"));
        candies.add(Candy.newInstance(	700306	,"oreo doble crema"));
        candies.add(Candy.newInstance(	701200	,"Pecosos"));
        candies.add(Candy.newInstance(	701302	,"Lacasito"));
        candies.add(Candy.newInstance(	701306	,"Lacasito chocobites"));
        candies.add(Candy.newInstance(	701901	,"Chocobolas blanca"));
        candies.add(Candy.newInstance(	701902	,"Chocobolas negra"));
        candies.add(Candy.newInstance(	702000	,"Conguitos"));
        candies.add(Candy.newInstance(	702001	,"Conguitos galleta"));
        candies.add(Candy.newInstance(	702002	,"Conguitos blancos"));
        candies.add(Candy.newInstance(	702136	,"Lacasitos 0630 100g 24u"));
        candies.add(Candy.newInstance(	702202	,"Almendra marcona"));
        candies.add(Candy.newInstance(	702203	,"Pistacho"));
        candies.add(Candy.newInstance(	702204	,"Nuez"));
        candies.add(Candy.newInstance(	702205	,"Almendra marcona blanca"));
        candies.add(Candy.newInstance(	702206	,"Pasas"));
        candies.add(Candy.newInstance(	702208	,"Dados naranja"));
        candies.add(Candy.newInstance(	702301	,"Avellana"));
        candies.add(Candy.newInstance(	702303	,"Almendras chocolate con leche"));
        candies.add(Candy.newInstance(	704414	,"Crunch"));
        candies.add(Candy.newInstance(	705400	,"Cacahuete blanc 5 kg"));
        candies.add(Candy.newInstance(	705401	,"Cacahuete dark Pink :D 5 kg"));
        candies.add(Candy.newInstance(	705402	,"Cacahuete Pink :D 5 kg"));
        candies.add(Candy.newInstance(	705403	,"Cacahuete Red :D 5 kg"));
        candies.add(Candy.newInstance(	705404	,"Cacahuete Aqua :D 5 kg"));
        candies.add(Candy.newInstance(	705405	,"Cacahuete verd 5 kg"));
        candies.add(Candy.newInstance(	705406	,"Cacahuete groc 5 kg	"));
        candies.add(Candy.newInstance(	705407	,"Cacahuete taronja 5 kg"));
        candies.add(Candy.newInstance(	705500	,"Cacahuete 1 kg"	));
        candies.add(Candy.newInstance(	705502	,"Choco 1 kg"));
        candies.add(Candy.newInstance(	705510	,"Twix miniatures 1 kg"));
        candies.add(Candy.newInstance(	705511	,"Mars miniatures 1 kg"));
        candies.add(Candy.newInstance(	705512	,"Snickers miniatures 1 kg"));
        candies.add(Candy.newInstance(	705514	,"Maltesers miniatures 1 kg"));
        candies.add(Candy.newInstance(	705865	,"Skittles Crazy Pouch"));
        candies.add(Candy.newInstance(	705866	,"Skittles Fruits Pouch"));
        candies.add(Candy.newInstance(	710060	,"Oreo"));
        candies.add(Candy.newInstance(	711312	,"Mini lacasito gnel."));
        candies.add(Candy.newInstance(	715801	,"Snickers"));
        candies.add(Candy.newInstance(	715802	,"Mars 2pack"));
        candies.add(Candy.newInstance(	716100	,"Twix"));
        candies.add(Candy.newInstance(	716400	,"Bounty"));
        candies.add(Candy.newInstance(	717400	,"Kit kat"));
        candies.add(Candy.newInstance(	717401	,"Kit kat dark"));
        candies.add(Candy.newInstance(	717402	,"Kit kat white"));
        candies.add(Candy.newInstance(	717403	,"Kit kat chunky"));
        candies.add(Candy.newInstance(	722308	,"Anacardos choco"));
        candies.add(Candy.newInstance(	722309	,"conguito colors (Kranch)"));
        candies.add(Candy.newInstance(	722370	,"Lacasito white"));
        candies.add(Candy.newInstance(	722408	,"Fresa"));
        candies.add(Candy.newInstance(	722409	,"Arandano"));
        candies.add(Candy.newInstance(	734007	,"Croissant bañado choco"));
        candies.add(Candy.newInstance(	734017	,"Croissant relleno choco"));
        candies.add(Candy.newInstance(	742515	,"Bueno white"));
        candies.add(Candy.newInstance(	742542	,"Sorpresa"));
        candies.add(Candy.newInstance(	742543	,"Bueno"));
        candies.add(Candy.newInstance(	744509	,"Delice"));
        candies.add(Candy.newInstance(	744570	,"Joy"));
        candies.add(Candy.newInstance(	800000	,"Moras gde"));
        candies.add(Candy.newInstance(	800001	,"Fresa Salvaje"));
        candies.add(Candy.newInstance(	800002	,"Corazón melocoton"));
        candies.add(Candy.newInstance(	800003	,"Platanos"));
        candies.add(Candy.newInstance(	800004	,"Botella cola (verda)"));
        candies.add(Candy.newInstance(	800006	,"Besos fresa	"));
        candies.add(Candy.newInstance(	800008	,"Botella cola (marró)"));
        candies.add(Candy.newInstance(	800012	,"Lagrimas"));
        candies.add(Candy.newInstance(	800013	,"Aros fresa"));
        candies.add(Candy.newInstance(	800014	,"Fresones"));
        candies.add(Candy.newInstance(	800015	,"Osos grande az"));
        candies.add(Candy.newInstance(	800016	,"Fresa slvj pica"));
        candies.add(Candy.newInstance(	800019	,"Tajada Sandia"));
        candies.add(Candy.newInstance(	800020	,"Mini osito brillo"	));
        candies.add(Candy.newInstance(	800023	,"Tiburón azul"));
        candies.add(Candy.newInstance(	800041	,"Gusanos pica"));
        candies.add(Candy.newInstance(	800053	,"Gatito regaliz"	));
        candies.add(Candy.newInstance(	800082	,"Maxifresapica"));
        candies.add(Candy.newInstance(	800110	,"Dedos brillo"));
        candies.add(Candy.newInstance(	800530	,"Relleno pica fresa"	));
        candies.add(Candy.newInstance(	822002	,"Pirul. Corazon"	));


        for (int i = 0; i < candies.size(); i++){
            Map<String, Object> data = new HashMap<>();
            data.put("id", candies.get(i).getId());
            data.put("name", candies.get(i).getName());
            db.collection("Candies").document(String.valueOf(candies.get(i).getId())).set(data);
        }

    }

    private void loadData() {
        db.collection("Candies")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){
                            for (QueryDocumentSnapshot documentSnapshot : task.getResult()){
                                Log.d("READING CANDIES", documentSnapshot.getId());
                                Candy candy = Candy.newInstance(Integer.parseInt(documentSnapshot.get("id").toString()), documentSnapshot.get("name").toString());
                                candies.add(candy);
                            }
                            progressDialog.hide();
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
    }
}
