package com.sgv.cinethingrious.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.DateTimeKeyListener;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.sgv.cinethingrious.R;
import com.tom_roush.pdfbox.pdmodel.PDDocument;
import com.tom_roush.pdfbox.text.PDFTextStripper;
import com.tom_roush.pdfbox.text.PDFTextStripperByArea;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.io.IOException;
import java.io.PipedReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import dmax.dialog.SpotsDialog;

public class UploadFileActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_FILES = 7193;
    private List<String> listNameFiles;
    private List<File> listFiles;
    private ArrayAdapter adapter;
    private Handler mHandler;
    private AlertDialog progressDialogInfo;
    private List<String> movies;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_file);
        mHandler = new Handler();
        db = FirebaseFirestore.getInstance();
        ListView listView = findViewById(R.id.list_files);
        listNameFiles = new ArrayList<>();
        listFiles = new ArrayList<>();
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listNameFiles);
        listView.setAdapter(adapter);
        progressDialogInfo = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage(0)
                .setTheme(0)
                .setCancelable(false)
                .build();
        loadData();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(UploadFileActivity.this, listFiles.get(position).getAbsolutePath(), Toast.LENGTH_SHORT).show();
                if (listFiles.get(position).isFile()){
                    if (listFiles.get(position).getName().contains(".pdf")){
                        startReadMovies(listFiles.get(position));
                    }else {
                        Log.d("SEL·LECCIÓ", "L'usuari no ha sel·leccionat cap fitxer pdf");
                        Toast.makeText(UploadFileActivity.this, "You must select a file with pdf's extension.", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Log.d("SEL·LECCIÓ", "L'usuari ha sel·leccionat una carpeta");
                    Toast.makeText(UploadFileActivity.this, "Select a file to upload the movies", Toast.LENGTH_SHORT).show();
                }
            }
        });
        FloatingActionButton fab = findViewById(R.id.fab_help_upload_file);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(UploadFileActivity.this, "Select the file that contains the billboard with the new movies. If you don't found the files, make sure the file was saved on Download's folder", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void startReadMovies(final File file){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("PROCESS UPLOAD MOVIES", "S'inicia procés");
                            progressDialogInfo.setMessage("Loading file...");
                            progressDialogInfo.show();
                        }
                    });
                    PDDocument document = PDDocument.load(file);
                    document.getClass();
                    if (!document.isEncrypted()){
                        PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                        stripper.setSortByPosition(true);
                        PDFTextStripper tStripper = new PDFTextStripper();
                        try {
                            Thread.sleep(1500);
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialogInfo.setMessage("Processing file...");
                                }
                            });
                        }catch (InterruptedException e){
                            Log.e("PROCESS UPLOAD MOVIES", "Excepció d'interrupció");
                        }
                        readAndTreat(tStripper.getText(document));
                        try {
                            Thread.sleep(1500);
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialogInfo.setMessage("Processing Movies");
                                }
                            });
                            readMoviesFromList();
                        }catch (InterruptedException e){
                            Log.e("PROCESS UPLOAD MOVIES", "Excepció d'interrupció");
                        }
                        try {
                            Thread.sleep(1500);
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialogInfo.setMessage("saving Movies to database");
                                }
                            });
                            saveMoviesList();
                        }catch (InterruptedException e){
                            Log.e("PROCESS UPLOAD MOVIES", "Excepció d'interrupció");
                        }
                    }
                }catch (IOException e){
                    Log.e("PROCESS UPLOAD MOVIES", "Error en la lectura del fitxer");
                }

            }
        }).start();
    }

    private void saveMoviesList() {
        Map<String, Object> data = new HashMap<>();

        DateTime dateTime = new DateTime();
        int daysToFriday = getDaysToFriday(dateTime);
        if (daysToFriday < 0){
            dateTime = dateTime.minusDays(Math.abs(daysToFriday));
        }else {
            dateTime = dateTime.plusDays(daysToFriday);
        }
        data.put("ID", getWeekOfBillboardToString(dateTime));
        data.put("Movies", movies);
        db.collection("Movies")
                .document(getWeekOfBillboardToString(dateTime))
                .set(data)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialogInfo.setMessage("Success. Finalizing...");
                            try {
                                Thread.sleep(1500);
                                progressDialogInfo.hide();
                            }catch (InterruptedException e){
                                Log.e("PROCESS UPLOAD MOVIES", "Excepció d'interrupció");
                            }
                        }
                    });
                }
        });
    }

    private String getWeekOfBillboardToString(DateTime dateTime) {

        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd-MM-yyyy");
        String week;
        week = dateTime.toString(fmt) + " | " + dateTime.plusDays(6).toString(fmt);
        //week = "22-03-2019 | 28-03-2019";
        return week;
    }

    private int getDaysToFriday(DateTime dateTime){
        int dayOfWeek = dateTime.getDayOfWeek();
        int numToFriday = 0;
        switch (dayOfWeek){
            case 1:
                numToFriday = 4;
                break;
            case 2:
                numToFriday = 3;
                break;
            case 3:
                numToFriday = 2;
                break;
            case 4:
                numToFriday = 1;
                break;
            case 5:
                numToFriday = 0;
                break;
            case 6:
                numToFriday = -1;
                break;
            case 7:
                numToFriday = -2;
                break;
        }
        return numToFriday;
    }

    private void readMoviesFromList() {
        List<String> auxMovies = new ArrayList<>();
        for (int i = 0; i < movies.size(); i++){
            String movie = movies.get(i);
            if (!auxMovies.contains(movie)){
                auxMovies.add(movie);
            }
        }
        movies.clear();
        movies.addAll(auxMovies);
        for (int i = 0; i < movies.size(); i++){
            movies.set(i, movies.get(i).substring(1));
        }
    }

    private void readAndTreat(String str) {
        movies = new ArrayList<>();
        int position = 0;
        Scanner scnLine = new Scanner(str);
        String line = "";
        String movie = "";
        while (scnLine.hasNext()){
            line = scnLine.nextLine();
            if (position == 2) {
                Scanner mScanner = new Scanner(line);
                while (mScanner.hasNext()){
                    String next = mScanner.next();
                    if (next.equals("ESP") || next.equals("CAT") || next.equals("VOSE")){
                        movie = movie + next;
                    }else {
                        movie = movie + " " + next;
                    }
                    if (movie.contains("HD-ESP")||movie.contains("HD-CAT")||movie.contains("3D-ESP")||movie.contains("3D-CAT")||movie.contains("HD-VOSE")){
                       movies.add(movie);
                       movie = "";
                       line = scnLine.nextLine();
                       while (noMovie(line) && scnLine.hasNext()){
                           line = scnLine.nextLine();
                       }
                       /*if (noMovie(line)){
                           line = scnLine.nextLine();
                       }
                       if (noMovie(line)){
                           line = scnLine.nextLine();
                       }
                       if (noMovie(line)){
                           line = scnLine.nextLine();
                       }*/
                       mScanner = new Scanner(line);
                    }
                }

            }else {
                position = position + 1;
            }
        }
    }

    private boolean noMovie(String line){
        if (line.contentEquals("ESP")){
            return false;
        }
        if (line.contentEquals("CAT")){
            return false;
        }
        if (line.contentEquals("VOSE")){
            return false;
        }
        if (line.contains("HD-ESP")){
            return false;
        }
        if (line.contains("HD-CAT")){
            return false;
        }
        if (line.contains("3D-ESP")){
            return false;
        }
        if (line.contains("3D-CAT")){
            return false;
        }
        if (line.contains("HD-VOSE")){
            return false;
        }
        if (line.contains("SESSIONS MATINALS")){
            return true;
        }
        if (line.contains("SESSIONS GOLFA")){
            return true;
        }
        if (line.contains("Programació i horaris")){
            return true;
        }
        if (line.contains("VOSE:") || line.contains("Documental:") || line.contains("ÒPERA:")){
            return true;
        }
        if (line.contains("DOCUMENTAL:")){
            return true;
        }
        if (line.contains("PRE-ESTRENA:")){
            return true;
        }
        if (line.contains("JCA CINEMES LLEIDA")){
            return true;
        }
        if (line.contains("MATINALS CADA DIA")){
            return true;
        }
        if (line.contentEquals(" ")){
            return true;
        }
        if (line.contains("Anys")){
            return true;
        }
        if (line.contains("Apta")){
            return true;
        }
        if (line.contains("Òpera")){
            return true;
        }
        if (line.contains("Acció")){
            return true;
        }
        if (line.contains("Drama")){
            return true;
        }
        if (line.contains("Animació")){
            return true;
        }
        if (line.contains("Comèdia")){
            return true;
        }
        if (line.contains("Thriller")){
            return true;
        }
        if (line.contains("Histórica")){
            return true;
        }
        if (line.contains("Fantasia")){
            return true;
        }
        if (line.contains("Aventures")){
            return true;
        }
        if (line.contains("Documental")){
            return true;
        }
        if (line.contains("Biopic")){
            return true;
        }
        if (line.contains("Anime")){
            return true;
        }
        if (line.contains("Terror")){
            return true;
        }
        if (line.contains("Ciencia Ficció")){
            return true;
        }
        if (line.contains("SALA 1,") || line.contains("SALA 2,") || line.contains("SALA 3,") || line.contains("SALA 4,") || line.contains("SALA 5,") || line.contains("SALA 6,") || line.contains("SALA 7,") || line.contains("SALA 8,") || line.contains("SALA 9,") || line.contains("SALA 10,") || line.contains("SALA 11,") || line.contains("SALA 12,") || line.contains("SALA 13,") || line.contains("SALA 14,") || line.contains("SALA 15,") || line.contains("SALA 16,")){
            return true;
        }
        return false;
    }
    private void loadData(){
        //listNameFiles = new ArrayList();
        //Toast.makeText(this, Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_DOWNLOADS, Toast.LENGTH_LONG).show();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_FILES);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else {
            getFiles();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_FILES: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getFiles();
                } else {
                    Toast.makeText(this, "You must accept the request permission for use this functionality.", Toast.LENGTH_SHORT).show();
                    finish();
                }
                return;
            }
        }
    }

    private void getFiles(){
        listNameFiles.clear();
        listFiles.clear();
        File downloadDirectory = new File(Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_DOWNLOADS);
        File[] listFile = downloadDirectory.listFiles();
        if (listFile.length > 0) {
            for (File file : listFile) {
                listNameFiles.add(file.getName());
                listFiles.add(file);
            }
        }
        Collections.sort(listNameFiles, String.CASE_INSENSITIVE_ORDER);
        Collections.sort(listFiles);
        adapter.notifyDataSetChanged();
    }
}
