package com.sgv.cinethingrious.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import com.sgv.cinethingrious.R;
import com.sgv.cinethingrious.model.Movie;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MoviesAdapter extends ArrayAdapter<Movie> implements Filterable{

    private Context context;
    private List<Movie> movieList;
    private List<Movie> movieListFiltered;

    private static class ViewHolder{
        TextView textTitle;
        TextView textDate;
    }

    public MoviesAdapter(Context context, List<Movie> data){
        super(context, R.layout.row_movie, data);
        this.context = context;
        this.movieList = data;
        this.movieListFiltered = movieList;
    }

    @Override
    public Movie getItem(int position) {
        return movieList.get(position);
    }

    @Override
    public int getCount() {
        return movieListFiltered.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Movie movie = getItem(position);
        ViewHolder viewHolder;
        final View result;
        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_movie, parent, false);
            viewHolder.textTitle = convertView.findViewById(R.id.text_title);
            viewHolder.textDate = convertView.findViewById(R.id.text_data);
            result = convertView;
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        viewHolder.textTitle.setText(movie.getTitle());
        viewHolder.textDate.setText(movie.getDate());
        return convertView;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    movieListFiltered = movieList;
                } else {
                    List<Movie> filteredList = new ArrayList<>();
                    for (Movie row : movieList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase()) || row.getDate().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    movieListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = movieListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                movieListFiltered = (ArrayList<Movie>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    //filter
    /*public void filter(String charText){
        charText = charText.toLowerCase(Locale.getDefault());
        movieList.clear();
        if (charText.length()==0){
            movieList.addAll(arrayList);
        }
        else {
            for (Movie model : arrayList){
                if (model.getTitle().toLowerCase(Locale.getDefault())
                        .contains(charText)){
                    movieList.add(model);
                }
            }
        }
        notifyDataSetChanged();
    }*/
}
