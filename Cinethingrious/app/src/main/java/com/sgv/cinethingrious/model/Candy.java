package com.sgv.cinethingrious.model;

public class Candy {
    private int id;
    private String name;

    public Candy(int id, String name){
        this.id = id;
        this.name = name;
    }

    public static Candy newInstance(int id, String name){
        return new Candy(id, name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Candy{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
