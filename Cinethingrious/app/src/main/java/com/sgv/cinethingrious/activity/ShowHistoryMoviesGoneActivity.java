package com.sgv.cinethingrious.activity;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sgv.cinethingrious.R;
import com.sgv.cinethingrious.fragment.ListMoviesFragment;

public class ShowHistoryMoviesGoneActivity extends AppCompatActivity implements ListMoviesFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_history_movies_gone);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.list_container, ListMoviesFragment.newInstance());
        ft.commit();

    }

    @Override
    public void onFragmentInteraction(String id) {
        Intent intent = new Intent(this, DetailListMoviesHistory.class);
        intent.putExtra("ID", id);
        startActivity(intent);
    }
}
