package com.sgv.cinethingrious.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sgv.cinethingrious.R;
import com.sgv.cinethingrious.model.Candy;

import java.util.List;

public class CandyAdapter extends BaseAdapter {

    Context context;
    private static LayoutInflater inflater = null;
    private List<Candy> candies;

    public CandyAdapter(Context context, List<Candy> data){
        this.context = context;
        this.candies = data;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return candies.size();
    }

    @Override
    public Candy getItem(int position) {
        return candies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return candies.get(position).getId();
    }

    public class HolderCandy{
        TextView idCandy, nameCandy;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HolderCandy holderCandy = new HolderCandy();
        View view = inflater.inflate(R.layout.row_candy, null);
        holderCandy.idCandy = view.findViewById(R.id.textview_id_candy);
        holderCandy.nameCandy = view.findViewById(R.id.textview_name_candy);
        holderCandy.idCandy.setText(String.valueOf(candies.get(position).getId()));
        holderCandy.nameCandy.setText(candies.get(position).getName());
        return view;
    }
}
