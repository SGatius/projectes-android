package com.sgv.cinethingrious.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.sgv.cinethingrious.R;
import com.sgv.cinethingrious.adapter.GridAdapter;

import java.util.ArrayList;
import java.util.List;

public class FunctionsActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private List<Integer> idImages;
    private List<String> nameFunctions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_functions);
        mAuth = FirebaseAuth.getInstance();
        Toast.makeText(this, "Welcome " + mAuth.getCurrentUser().getEmail(), Toast.LENGTH_SHORT).show();
        loadData();
        GridView gridView = findViewById(R.id.grid_view);
        gridView.setAdapter(new GridAdapter(this, nameFunctions, idImages));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).toString().equals(nameFunctions.get(0))) {
                    //startActivity(new Intent(FunctionsActivity.this, UploadFileActivity.class));
                    startActivity(new Intent(FunctionsActivity.this, HistoryMoviesActivity.class));
                }
                if (parent.getItemAtPosition(position).toString().equals(nameFunctions.get(1))){
                    //startActivity(new Intent(FunctionsActivity.this, ShowMoviesLeaveActivity.class));
                    //startActivity(new Intent(FunctionsActivity.this, MoviesBillboardActivity.class));
                }
                if (parent.getItemAtPosition(position).toString().equals(nameFunctions.get(2))){
                    //startActivity(new Intent(FunctionsActivity.this, ShowMoviesEnterActivity.class));
                    startActivity(new Intent(FunctionsActivity.this, UploadMoviesActivity.class));
                }
                /*if (parent.getItemAtPosition(position).toString().equals(nameFunctions.get(3))){
                    startActivity(new Intent(FunctionsActivity.this, ShowHistoryMoviesGoneActivity.class));
                }
                if (parent.getItemAtPosition(position).toString().equals(nameFunctions.get(4))){
                    startActivity(new Intent(FunctionsActivity.this, ShowHistoryMoviesEnterActivity.class));
                }*/
                if (parent.getItemAtPosition(position).toString().equals(nameFunctions.get(3))){
                    startActivity(new Intent(FunctionsActivity.this, ListCandyShopActivity.class));
                }
            }
        });
    }

    private void loadData(){
        idImages = new ArrayList<>();
        nameFunctions = new ArrayList<>();
        nameFunctions.add("History of movies");
        idImages.add(R.drawable.ic_history_movies);
        nameFunctions.add("Movies on billboard");
        idImages.add(R.drawable.ic_movies_billboard);
        nameFunctions.add("Upload Movies");
        idImages.add(R.mipmap.ic_upload_movies);
        /*nameFunctions.add(getString(R.string.function_load_movies));
        idImages.add(R.mipmap.ic_upload_billboard);
        nameFunctions.add(getString(R.string.function_outgoing_films));
        idImages.add(R.mipmap.ic_latest_movies_in_billboard);
        nameFunctions.add(getString(R.string.function_incoming_movies_week));
        idImages.add(R.mipmap.ic_movies_enter);
        nameFunctions.add(getString(R.string.function_text_history_outgoing));
        idImages.add(R.mipmap.ic__history_movies_gone_away);
        nameFunctions.add(getString(R.string.function_incoming_movies));
        idImages.add(R.mipmap.ic__history_movies_enter);*/
        nameFunctions.add(getString(R.string.function_list_candies));
        idImages.add(R.mipmap.ic_oso_color_gominola);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}
