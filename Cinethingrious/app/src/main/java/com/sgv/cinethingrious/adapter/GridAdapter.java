package com.sgv.cinethingrious.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sgv.cinethingrious.R;

import java.util.List;

public class GridAdapter extends BaseAdapter {
    Context context;
    List<String> nameFunctions;
    List<Integer> idImages;
    private static LayoutInflater inflater = null;

    public GridAdapter(Context context, List<String> nameFunctions, List<Integer> idImages) {
        this.context = context;
        this.nameFunctions = nameFunctions;
        this.idImages = idImages;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return nameFunctions.size();
    }

    @Override
    public String getItem(int position) {
        return nameFunctions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder{
        TextView textView;
        ImageView imageView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.grid_layout, null);
        holder.textView = rowView.findViewById(R.id.function_texts);
        holder.imageView = rowView.findViewById(R.id.function_images);
        holder.textView.setText(nameFunctions.get(position));
        holder.imageView.setImageResource(idImages.get(position));
        return rowView;
    }
}
