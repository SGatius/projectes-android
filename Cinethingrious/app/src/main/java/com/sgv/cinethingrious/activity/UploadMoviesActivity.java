package com.sgv.cinethingrious.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.sgv.cinethingrious.R;
import com.sgv.cinethingrious.model.Movie;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Calendar;

import dmax.dialog.SpotsDialog;

public class UploadMoviesActivity extends AppCompatActivity {

    private TextInputLayout titleMovie, dateMovie;
    private Spinner spinner;
    private String formatMovie;
    private String date;
    private DateTimeFormatter fmt;
    private FirebaseFirestore db;
    private AlertDialog progressDialogInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_movies);

        fmt = DateTimeFormat.forPattern("dd/MM/yyyy");
        formatMovie = "ESP";
        date = new DateTime().toString(fmt);
        titleMovie = findViewById(R.id.til_title);
        DatePicker datePicker = findViewById(R.id.date_movie);
        db = FirebaseFirestore.getInstance();
        progressDialogInfo = new SpotsDialog.Builder()
                .setContext(this)
                .setMessage(0)
                .setTheme(0)
                .setCancelable(false)
                .build();

        datePicker.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                DateTime dateTime = new DateTime(view.getYear(), view.getMonth() + 1, view.getDayOfMonth(), 0, 0);
                date = dateTime.toString(fmt);
            }
        });

        spinner = (Spinner) findViewById(R.id.spinner_version_movies);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.movies_format_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                formatMovie = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        findViewById(R.id.fab_upload_movie).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = titleMovie.getEditText().getText().toString() + " " + formatMovie;
                final Movie movie = new Movie(title, date);
                progressDialogInfo.setMessage("Uploading " + movie.getTitle() + "...");
                progressDialogInfo.show();
                db.collection("MoviesBillboard").document().set(movie).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        db.collection("MoviesHistory").document().set(movie).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                progressDialogInfo.hide();
                                Toast.makeText(UploadMoviesActivity.this, movie.getTitle() + " was uploaded", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
            }
        });
    }
}
