package com.sgv.sonylamp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

//import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.messaging.FirebaseMessaging;
import com.sgv.sonylamp.adapter.LampAdapter;
import com.sgv.sonylamp.model.Lamp;
import com.sgv.sonylamp.service.MyService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private List<Lamp> list;
    private LampAdapter adapter;
    private FirebaseFirestore db;
    private static String Collection = "auditoriums";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseMessaging.getInstance().subscribeToTopic("warningLamps");
        //startService(new Intent(this, MyService.class));

        list = new ArrayList<>();
        adapter = new LampAdapter(list, this);
        db = FirebaseFirestore.getInstance();

        listView = findViewById(R.id.list_audit_lamps);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showDialogLamp(R.layout.dialog_lamp, position);
            }
        });

        loadData();
    }

    private void showDialogLamp(int dialog_lamp, final int position) {
        final Lamp lamp = list.get(position);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        View layoutView = getLayoutInflater().inflate(dialog_lamp, null);
        Button btn = layoutView.findViewById(R.id.btnDialog);
        TextView numAuditorium = layoutView.findViewById(R.id.num_auditorium);
        numAuditorium.setText(String.valueOf(lamp.getAuditorium()));

        final EditText a1 = layoutView.findViewById(R.id.edit_a1);
        a1.setText(String.valueOf(lamp.getA1()));
        final EditText a2 = layoutView.findViewById(R.id.edit_a2);
        a2.setText(String.valueOf(lamp.getA2()));
        final EditText a3 = layoutView.findViewById(R.id.edit_a3);
        a3.setText(String.valueOf(lamp.getA3()));

        final EditText b1 = layoutView.findViewById(R.id.edit_b1);
        b1.setText(String.valueOf(lamp.getB1()));
        final EditText b2 = layoutView.findViewById(R.id.edit_b2);
        b2.setText(String.valueOf(lamp.getB2()));
        final EditText b3 = layoutView.findViewById(R.id.edit_b3);
        b3.setText(String.valueOf(lamp.getB3()));

        dialogBuilder.setView(layoutView);
        final AlertDialog dialog = dialogBuilder.create();
        dialog.show();
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lamp.setA1(Long.valueOf(a1.getText().toString()));
                lamp.setA2(Long.valueOf(a2.getText().toString()));
                lamp.setA3(Long.valueOf(a3.getText().toString()));
                lamp.setB1(Long.valueOf(b1.getText().toString()));
                lamp.setB2(Long.valueOf(b2.getText().toString()));
                lamp.setB3(Long.valueOf(b3.getText().toString()));
                Map<String, Object> data = new HashMap<>();
                data.put("Sala", lamp.getAuditorium());
                data.put("A1", lamp.getA1());
                data.put("A2", lamp.getA2());
                data.put("A3", lamp.getA3());
                data.put("B1", lamp.getB1());
                data.put("B2", lamp.getB2());
                data.put("B3", lamp.getB3());
                list.set(position, lamp);
                adapter.notifyDataSetChanged();
                db.collection(Collection).document(lamp.getDocumentID()).set(data).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(MainActivity.this, lamp.getAuditorium() + " was updated", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    private void loadData() {
        db.collection("auditoriums")
                .orderBy("Sala")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){

                            for (QueryDocumentSnapshot document : task.getResult()){

                                Map<String, Object> map = document.getData();

                                Lamp lamp = new Lamp((long)map.get("Sala"));
                                lamp.setDocumentID(document.getId());
                                lamp.setA1((long)map.get("A1"));
                                lamp.setA2((long)map.get("A2"));
                                lamp.setA3((long)map.get("A3"));
                                lamp.setB1((long)map.get("B1"));
                                lamp.setB2((long)map.get("B2"));
                                lamp.setB3((long)map.get("B3"));
                                list.add(lamp);
                            }
                            adapter.notifyDataSetChanged();

                        }else {
                            Toast.makeText(MainActivity.this, "Error en la lectura de la base de datos", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

}
