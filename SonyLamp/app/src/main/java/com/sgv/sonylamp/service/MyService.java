package com.sgv.sonylamp.service;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.sgv.sonylamp.broadcast.RestartUpdateDataService;
import com.sgv.sonylamp.observerandlistener.TimeListener;
import com.sgv.sonylamp.observerandlistener.TimeObservable;

import java.time.LocalTime;

public class MyService extends Service {

    private static String TAG = "SERVICE UPDATE DATABASE";
    private Thread thread;

    public MyService() {
    }

    @TargetApi(26)
    @Override
    public void onCreate(){
        TimeObservable observable = new TimeObservable(LocalTime.of(1,0,0));
        TimeListener listener = new TimeListener();
        observable.addObserver(listener);
        thread = new Thread(observable);
        thread.start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        super.onStartCommand(intent, flags, startId);
        //onTaskRemoved(intent);
        Log.d(TAG, "Service is running on foreground");
        //Toast.makeText(this, "Service is running", Toast.LENGTH_SHORT).show();
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }


    @Override
    public void onDestroy() {
        thread.interrupt();
        if (thread.isInterrupted()) {
            Log.d(TAG, "Service done");
        }
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("restartService");
        broadcastIntent.setClass(this, RestartUpdateDataService.class);
        this.sendBroadcast(broadcastIntent);
    }
}
