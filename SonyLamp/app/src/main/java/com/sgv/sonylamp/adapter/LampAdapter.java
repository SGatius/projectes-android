package com.sgv.sonylamp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.sgv.sonylamp.R;
import com.sgv.sonylamp.model.Lamp;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


public class LampAdapter extends ArrayAdapter<Lamp> {

    private List<Lamp> data;
    Context mContext;

    private static class ViewHolder{
        TextView numAuditorium, a1, a2, a3, b1, b2, b3;
    }

    public LampAdapter(List<Lamp> data, Context context){
        super(context, R.layout.row_lamp, data);
        this.data = data;
        this.mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Lamp lamp = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_lamp, parent, false);
            viewHolder.numAuditorium = (TextView)convertView.findViewById(R.id.num_auditorium);
            viewHolder.a1 = (TextView)convertView.findViewById(R.id.text_a1);
            viewHolder.a2 = (TextView)convertView.findViewById(R.id.text_a2);
            viewHolder.a3 = (TextView)convertView.findViewById(R.id.text_a3);
            viewHolder.b1 = (TextView)convertView.findViewById(R.id.text_b1);
            viewHolder.b2 = (TextView)convertView.findViewById(R.id.text_b2);
            viewHolder.b3 = (TextView)convertView.findViewById(R.id.text_b3);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.numAuditorium.setText(String.valueOf(lamp.getAuditorium()));
        viewHolder.a1.setText("A1: " + String.valueOf(lamp.getA1()));
        if (lamp.getA1() > 6500){
            viewHolder.a1.setTextColor(Color.YELLOW);
        }
        viewHolder.a2.setText("A2: " + String.valueOf(lamp.getA2()));
        if (lamp.getA2() > 6500){
            viewHolder.a2.setTextColor(Color.YELLOW);
        }
        viewHolder.a3.setText("A3: " + String.valueOf(lamp.getA3()));
        if (lamp.getA3() > 6500){
            viewHolder.a3.setTextColor(Color.YELLOW);
        }
        viewHolder.b1.setText("B1: " + String.valueOf(lamp.getB1()));
        if (lamp.getB1() > 6500){
            viewHolder.b1.setTextColor(Color.YELLOW);
        }
        viewHolder.b2.setText("B2: " + String.valueOf(lamp.getB2()));
        if (lamp.getB2() > 6500){
            viewHolder.b2.setTextColor(Color.YELLOW);
        }
        viewHolder.b3.setText("B3: " + String.valueOf(lamp.getB3()));
        if (lamp.getB3() > 6500){
            viewHolder.b3.setTextColor(Color.YELLOW);
        }

        return convertView;

    }

}
