package com.sgv.sonylamp.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.sgv.sonylamp.service.MyService;

public class RestartUpdateDataService extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("SERVICE", "Service restarted");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            context.startForegroundService(new Intent(context, MyService.class));
        }else {
            context.startService(new Intent(context, MyService.class));
        }
    }
}
