package com.sgv.sonylamp.observerandlistener;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import androidx.annotation.NonNull;

public class TimeListener implements Observer {

    @Override
    public void update(Observable o, Object arg) {

        if(arg instanceof Boolean){
            if((Boolean) arg){
                System.out.println("Se ha observado el tiempo especificado");
                incrementHoursToDB();
            }
        }
    }

    private void incrementHoursToDB(){
        final FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("auditoriums")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){
                            for (QueryDocumentSnapshot snapshots : task.getResult()){
                                DocumentReference ref = db.collection("auditoriums").document(snapshots.getId());
                                Map<String, Object> dataUpdate = new HashMap<>();
                                dataUpdate.put("A1", FieldValue.increment(12));
                                dataUpdate.put("A2", FieldValue.increment(12));
                                dataUpdate.put("A3", FieldValue.increment(12));
                                dataUpdate.put("B1", FieldValue.increment(12));
                                dataUpdate.put("B2", FieldValue.increment(12));
                                dataUpdate.put("B3", FieldValue.increment(12));
                                ref.update(dataUpdate);
                            }
                        }
                    }
                });
    }
}
