package com.sgv.sonylamp.observerandlistener;

import android.annotation.TargetApi;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Observable;

public class TimeObservable extends Observable implements Runnable {
    private LocalTime timeDecided;
    private Boolean isDetected;

    public TimeObservable(LocalTime time){
        this.timeDecided = time;
        this.isDetected = false;
    }

    @TargetApi(26)
    @Override
    public void run(){

        String pattern = "HH:mm:ss";
        DateTimeFormatter format = DateTimeFormatter.ofPattern(pattern);
        while(true){
            LocalTime localTime = LocalTime.now();

            if (localTime.format(format).equals(timeDecided.format(format))) {
                if(!isDetected){
                    isDetected = true;
                    setChanged();
                    notifyObservers(isDetected);
                }
            } else {
                if(isDetected){
                    isDetected = false;
                    setChanged();
                    notifyObservers(isDetected);
                }
            }

        }
    }
}
