package com.sgv.sonylamp.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.widget.RemoteViews;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sgv.sonylamp.R;
import com.sgv.sonylamp.SplashScreen;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

public class FirebaseMessageReceiver extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0){
            String title = getString(R.string.app_name);
            String message = remoteMessage.getData().get("sala") + " " + remoteMessage.getData().get("lamp") + " " + remoteMessage.getData().get("hours");
            showNotification(title, message);
        }
        if (remoteMessage.getNotification() != null){
            showNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
        }
    }

    private RemoteViews getCustomDesign(String title, String message){
        SimpleDateFormat fmt = new SimpleDateFormat("HH:mm", Locale.getDefault());
        String time = fmt.format(new Date());
        RemoteViews remoteViews = new RemoteViews(getApplicationContext().getPackageName(), R.layout.notification);
        remoteViews.setTextViewText(R.id.title, title);
        remoteViews.setTextViewText(R.id.message, message);
        remoteViews.setTextViewText(R.id.time, time);
        remoteViews.setImageViewResource(R.id.icon, R.mipmap.ic_not);
        return remoteViews;
    }

    public void showNotification(String title, String message){
        Intent intent = new Intent(this, SplashScreen.class);
        String channel_id = "lamps_warnings";
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri customSound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.r2d2_notification);
       //Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String GROUP_KEY_SONY_LAMP = "com.android.example.WORK_EMAIL";
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), channel_id)
                .setSmallIcon(R.mipmap.ic_not)
                .setSound(customSound)
                .setAutoCancel(true)
                .setVibrate(new long[]{1000,1000,1000})
                .setOnlyAlertOnce(true)
                .setGroup(GROUP_KEY_SONY_LAMP)
                .setGroupSummary(true)
                .setLights(Color.CYAN, 1000, 1000)
                .setContentIntent(pendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            builder = builder.setContent(getCustomDesign(title, message));
        }else {
            builder = builder.setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.ic_not);
        }
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(channel_id, "warning_lamps", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setSound(customSound, null);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        notificationManager.notify(0, builder.build());
    }
}
